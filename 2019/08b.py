#!/usr/bin/env python3
"""
"""

fh = open("08.txt", "r")
content = fh.read().strip()
fh.close()
#content = "3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0"
#content = "3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0"
content = [int(x) for x in content]

width = 25
height = 6

pixels_per_layer = width*height

layers = [content[layer*pixels_per_layer:(layer+1)*pixels_per_layer] for layer in range(len(content)//pixels_per_layer)]

TRANSPARENT = 2
WHITE = 1
BLACK = 0

zipped_pixels = zip(*layers)
result = []
for index, pixels in enumerate(zipped_pixels):
    single_result = None
    for pixel in pixels:
        if pixel == TRANSPARENT:
            continue
        elif pixel == WHITE:
            single_result = "#"
            break
        elif pixel == BLACK:
            single_result = "."
            break
        else:
            raise Exception("")
    if index % width == 0:
        result.append("\n")
    result.append(single_result)

print(''.join(result))

