#!/usr/bin/env python3

fh = open("03.txt", "r")
wires1 = fh.readline().strip().split(",")
wires2 = fh.readline().strip().split(",")
fh.close()

def get_points(wires):
    result = dict()
    pos = (0,0)
    steps = 0
    for wire in wires:
        command = wire[0]
        length = int(wire[1:])
        if command == "D":
            for i in range(1,length+1):
                pos = (pos[0]+1, pos[1])
                steps += 1
                result[pos] = steps
        elif command == "U":
            for i in range(1,length+1):
                pos = (pos[0]-1, pos[1])
                steps += 1
                result[pos] = steps
        elif command == "L":
            for i in range(1,length+1):
                pos = (pos[0], pos[1]-1)
                steps += 1
                result[pos] = steps
        elif command == "R":
            for i in range(1,length+1):
                pos = (pos[0], pos[1]+1)
                steps += 1
                result[pos] = steps
        else:
            raise Exception("Unknown command " + command)
    return result

def calc(wires1, wires2):
    points1 = get_points(wires1)
    points2 = get_points(wires2)
    crossings = set(points1.keys()).intersection(set(points2.keys()))
    min_steps = 99999999999
    for c in crossings:
        steps1 = points1.get(c)
        steps2 = points2.get(c)
        min_steps = min(min_steps, steps1+steps2)
    print(min_steps)


t1 = "R8,U5,L5,D3".split(",")
t2 = "U7,R6,D4,L4".split(",")
calc(t1, t2)


calc(wires1, wires2)
