#!/usr/bin/env python3

from collections import defaultdict

fh = open("14.txt")
lines = [s.strip() for s in fh.readlines()]
fh.close()
reactions = []
for line in lines:
    (before, after) = line.split(" => ")
    before = [e.split(" ") for e in before.split(", ")]
    after = after.split(" ")
    befores = []
    for item in before:
        befores.append((int(item[0]), item[1]))
    reactions.append((befores, int(after[0]), after[1]))

print("digraph d14 {")
for (befores, _, after) in reactions:
    for (_, before) in befores:
        print(before + " -> " + after + ";")
print("}");
