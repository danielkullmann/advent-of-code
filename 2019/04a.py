#!/usr/bin/env python3
"""
"""

def check(num):
    correct = False
    for i in range(0,len(num)-1):
        d1 = num[i]
        d2 = num[i+1]
        if d1 > d2: return False
        if d1 == d2: correct = True
    return correct


count = 0
for num in range(235741,706948+1):
    if check(str(num)): count += 1

print(count)
