#!/usr/bin/env python3
"""
"""

def parse_op_code(input):
    opcode = input % 100
    input = input // 100
    imm1 = (input % 10) == 1
    input = input // 10
    imm2 = (input % 10) == 1
    input = input // 10
    imm3 = (input % 10) == 1
    return (opcode, imm1, imm2, imm3)

assert(parse_op_code(1002) == (2, False, True, False))

def get(content, value, immediate):
    if immediate:
        return value
    else:
        return content[value]

def run(content):
    pc = 0
    output = ""
    while True:
        (opcode, imm1, imm2, imm3) = parse_op_code(content[pc])
        if opcode == 1:
            # addition
            p1 = content[pc+1]
            p2 = content[pc+2]
            p3 = content[pc+3]
            content[p3] = get(content, p1, imm1) + get(content, p2, imm2)
            pc += 4
        elif opcode == 2:
            # multiplication
            p1 = content[pc+1]
            p2 = content[pc+2]
            p3 = content[pc+3]
            content[p3] = get(content, p1, imm1) * get(content, p2, imm2)
            pc += 4
        elif opcode == 3:
            p1 = content[pc+1]
            content[p1] = 5
            pc += 2
        elif opcode == 4:
            p1 = content[pc+1]
            output += str(get(content, p1, imm1)) + "\n"
            pc += 2
        elif opcode == 5:
            p1 = content[pc+1]
            p2 = content[pc+2]
            if get(content, p1, imm1) != 0:
                pc = get(content, p2, imm2)
            else:
                pc += 3
        elif opcode == 6:
            p1 = content[pc+1]
            p2 = content[pc+2]
            if get(content, p1, imm1) == 0:
                pc = get(content, p2, imm2)
            else:
                pc += 3
        elif opcode == 7:
            p1 = content[pc+1]
            p2 = content[pc+2]
            p3 = content[pc+3]
            if get(content, p1, imm1) < get(content, p2, imm2):
                content[p3] = 1
            else:
                content[p3] = 0
            pc += 4
        elif opcode == 8:
            p1 = content[pc+1]
            p2 = content[pc+2]
            p3 = content[pc+3]
            if get(content, p1, imm1) == get(content, p2, imm2):
                content[p3] = 1
            else:
                content[p3] = 0
            pc += 4
        elif opcode == 99:
            # program exit
            break
        else:
            raise Exception("Unknown opcode " + str(opcode))
    return output

fh = open("05.txt", "r")
content = fh.read()
fh.close()
content = [int(x) for x in content.split(",")]

#content = [int(x) for x in "3,9,8,9,10,9,4,9,99,-1,8".split(",")]

print(run(content))
