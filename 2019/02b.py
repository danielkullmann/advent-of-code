#!/usr/bin/env python3
"""
"""

def run(content):
    pc = 0
    while True:
        opcode = content[pc]
        if opcode == 1:
            # addition
            p1 = content[pc+1]
            p2 = content[pc+2]
            p3 = content[pc+3]
            content[p3] = content[p1] + content[p2]
            pc += 4
        elif opcode == 2:
            # multiplication
            p1 = content[pc+1]
            p2 = content[pc+2]
            p3 = content[pc+3]
            content[p3] = content[p1] * content[p2]
            pc += 4
        elif opcode == 99:
            # program exit
            break
        else:
            raise Exception("Unknown opcode " + str(opcode))
    return content

fh = open("02.txt", "r")
content = fh.read()
fh.close()
original_content = [int(x) for x in content.split(",")]

for noun in range(0,100):
    for verb in range(0,100):
        content = original_content[:]
        content[1] = noun
        content[2] = verb
        content = run(content)
        if content[0] == 19690720:
            print(100*noun+verb)
            break
