#!/usr/bin/env python3
"""
"""

from math import gcd

fh = open("10.txt", "r")
content = fh.read()
fh.close()
#content = ".#..#\n.....\n#####\n....#\n...##"
lines = content.strip().split("\n")


asteroids = set()
for (y, line) in enumerate(lines):
    for (x, c) in enumerate(line):
        if c == "#":
            asteroids.add((y,x))

print(len(asteroids))
#assert((0,0) not in asteroids)
#assert((1,0) in asteroids)
#assert((0,1) in asteroids)

def sign(value):
    if value == 0:
        return 0
    if value > 0:
        return +1
    if value < 0:
        return -1

max_num = None
max_asteroid = None
for (y0,x0) in asteroids:
    num = 0
    print("## Check " + str((y0,x0)))
    for (y1,x1) in asteroids:
        print("  against " + str((y1,x1)))
        dx = x1-x0
        dy = y1-y0
        if dx == 0 and dy == 0:
            print("    same same")
            continue # same asteroid; not counting
        elif dx == 0:
            print("    same x")
            dy = sign(dy)
        elif dy == 0:
            print("    same y")
            dx = sign(dx)
        else:
            dd = gcd(dx, dy)
            odx = dx
            ody = dy
            dx //= dd
            dy //= dd
            print("    other: " + str((dd, dy, dx, ody, odx)))
        x = x0+dx
        y = y0+dy
        has_direct_line_of_sight = True
        while x != x1 or y != y1:
            print("    asteroid " + str((y,x,(y,x) in asteroids)))
            if (y,x) in asteroids:
                has_direct_line_of_sight = False
                break
            x += dx
            y += dy
        if has_direct_line_of_sight:
            num += 1
    if max_num is None or num > max_num:
        max_num = num
        max_asteroid = (y0,x0)
        print("> " + str(max_num) + " " + str(max_asteroid))

print(max_num, max_asteroid)


