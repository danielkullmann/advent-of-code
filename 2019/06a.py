"""
"""

fh = open("06.txt")
orbit_definitions = [line.split(")") for line in fh.read().strip().split("\n")]
fh.close()

# orbit_definitions = [
#     line.split(")") 
#     for line in "COM)B\nB)C\nC)D\nD)E\nE)F\nB)G\nG)H\nD)I\nE)J\nJ)K\nK)L".split("\n")
# ]

root = "COM"
queue = [(root, 0)]
num_orbits = 0
while len(queue) > 0:
    (node, depth) = queue.pop()
    num_orbits += depth
    orbiters = [b for (a,b) in orbit_definitions if a == node]
    queue.extend([(orbiter, depth+1) for orbiter in orbiters])

print(num_orbits)