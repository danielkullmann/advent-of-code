#!/usr/bin/env python3

import re

REGEX = re.compile("^<x=(-?[0-9]+), y=(-?[0-9]+), z=(-?[0-9]+)>")

def parse(line):
    match = REGEX.match(line)
    return list(map(int,match.groups()))

fh = open("12.txt")
positions = [parse(line) for line in fh.read().strip().split("\n")]
fh.close()
velocities = [[0,0,0], [0,0,0], [0,0,0], [0,0,0]]
#positions = [parse(line) for line in "<x=-1, y=0, z=2>\n<x=2, y=-10, z=-7>\n<x=4, y=-8, z=8>\n<x=3, y=5, z=-1>".split("\n")]


for step in range(1000):
    for i1 in range(len(positions)):
        for i2 in range(len(positions)):
            if i1 == i2: continue
            for coord in range(3):
                if positions[i1][coord] < positions[i2][coord]:
                    velocities[i1][coord] += 1
                elif positions[i1][coord] > positions[i2][coord]:
                    velocities[i1][coord] -= 1
    for i1 in range(len(positions)):
        for coord in range(3):
            positions[i1][coord] += velocities[i1][coord]

    #print("# " + str(step+1))
    #print(positions)
    #print(velocities)

    #if step==9:
    #    break

energy = 0
for i in range(len(positions)):
    potential = sum(map(abs, positions[i]))
    kinetic = sum(map(abs, velocities[i]))
    energy += potential * kinetic

print(energy)
