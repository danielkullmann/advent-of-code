#!/usr/bin/env python3
"""
"""
from collections import defaultdict
from math import atan2, gcd, pi

fh = open("10.txt", "r")
content = fh.read()
fh.close()
content2 = """
.#..##.###...#######
##.############..##.
.#.######.########.#
.###.#######.####.#.
#####.##.#.##.###.##
..#####..#.#########
####################
#.####....###.#.#.##
##.#################
#####.##.###..####..
..######..##.#######
####.##.####...##..#
.#####..#.######.###
##...#.##########...
#.##########.#######
.####.#.###.###.#.##
....##.##.###..#####
.#.#.###########.###
#.#.#.#####.####.###
###.##.####.##.#..##"""
lines = content.strip().split("\n")


asteroids = set()
for (y, line) in enumerate(lines):
    for (x, c) in enumerate(line):
        if c == "#":
            asteroids.add((x,y))

def sign(value):
    if value == 0:
        raise Exception("sign")
        #return 0
    if value > 0:
        return +1
    if value < 0:
        return -1

max_num = None
max_asteroid = None
for (x0,y0) in asteroids:
    num = 0
    #print("## Check " + str((y0,x0)))
    for (x1,y1) in asteroids:
        #print("  against " + str((y1,x1)))
        dx = x1-x0
        dy = y1-y0
        if dx == 0 and dy == 0:
            #print("    same same")
            continue # same asteroid; not counting
        elif dx == 0:
            #print("    same x")
            dy = sign(dy)
        elif dy == 0:
            #print("    same y")
            dx = sign(dx)
        else:
            dd = gcd(dx, dy)
            dx //= dd
            dy //= dd
            #print("    other: " + str((dd, dy, dx)))
        x = x0+dx
        y = y0+dy
        has_direct_line_of_sight = True
        while x != x1 or y != y1:
            #print("    asteroid " + str((y,x,(y,x) in asteroids)))
            if (x,y) in asteroids:
                has_direct_line_of_sight = False
                break
            x += dx
            y += dy
        if has_direct_line_of_sight:
            num += 1
    if max_num is None or num > max_num:
        max_num = num
        max_asteroid = (x0,y0)
        #print("> " + str(max_num) + " " + str(max_asteroid))

print(max_num, max_asteroid)
(x0, y0) = max_asteroid

def calculate_angle(dx, dy):
    radians = atan2(dy, dx) + pi/2
    if radians < 0:
        radians += 2*pi
    return radians

assert(calculate_angle(1, 0) == pi/2)
assert(calculate_angle(-1, 0) == 3*pi/2)
assert(calculate_angle(0, -1) == 0)
assert(calculate_angle(0, 1) == pi)

# key: angle
# value: list of (distance, (y,x) of asteroid)
asteroid_map = defaultdict(list)
for (x1,y1) in asteroids:
    #print("  against " + str((y1,x1)))
    dx = x1-x0
    dy = y1-y0
    if dx == 0 and dy == 0: continue
    angle = calculate_angle(dx, dy)
    asteroid_map[angle].append((abs(dx)+abs(dy),(x1,y1)))

keys = sorted(asteroid_map.keys())
#print(keys)
sorted_asteroid_map = {}
for key in keys:
    sorted_asteroid_map[key] = sorted(asteroid_map[key])
    #print(key, sorted_asteroid_map[key])

idx = 0
while True:
    if idx == 200: break
    for key in keys:
        asteroids_in_line = sorted_asteroid_map[key]
        if len(asteroids_in_line) > 0:
            asteroid = asteroids_in_line.pop(0)
            idx += 1
            #print(idx, key, asteroid)
            if idx == 200:
                print("b: ", asteroid)
                break

