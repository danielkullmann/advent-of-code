#!/usr/bin/env python3
"""
"""

def parse_op_code(input):
    opcode = input % 100
    input = input // 100
    imm1 = (input % 10) == 1
    input = input // 10
    imm2 = (input % 10) == 1
    input = input // 10
    imm3 = (input % 10) == 1
    return (opcode, imm1, imm2, imm3)

assert(parse_op_code(1002) == (2, False, True, False))

def get(content, value, immediate):
    if immediate:
        return value
    else:
        return content[value]

def run(content, input):
    content = content[:]
    pc = 0
    inputIndex = 0
    output = None
    while True:
        (opcode, imm1, imm2, imm3) = parse_op_code(content[pc])
        if opcode == 1:
            # addition
            p1 = content[pc+1]
            p2 = content[pc+2]
            p3 = content[pc+3]
            content[p3] = get(content, p1, imm1) + get(content, p2, imm2)
            pc += 4
        elif opcode == 2:
            # multiplication
            p1 = content[pc+1]
            p2 = content[pc+2]
            p3 = content[pc+3]
            content[p3] = get(content, p1, imm1) * get(content, p2, imm2)
            pc += 4
        elif opcode == 3:
            p1 = content[pc+1]
            content[p1] = input[inputIndex]
            inputIndex += 1
            pc += 2
        elif opcode == 4:
            p1 = content[pc+1]
            output = get(content, p1, imm1)
            pc += 2
        elif opcode == 5:
            p1 = content[pc+1]
            p2 = content[pc+2]
            if get(content, p1, imm1) != 0:
                pc = get(content, p2, imm2)
            else:
                pc += 3
        elif opcode == 6:
            p1 = content[pc+1]
            p2 = content[pc+2]
            if get(content, p1, imm1) == 0:
                pc = get(content, p2, imm2)
            else:
                pc += 3
        elif opcode == 7:
            p1 = content[pc+1]
            p2 = content[pc+2]
            p3 = content[pc+3]
            if get(content, p1, imm1) < get(content, p2, imm2):
                content[p3] = 1
            else:
                content[p3] = 0
            pc += 4
        elif opcode == 8:
            p1 = content[pc+1]
            p2 = content[pc+2]
            p3 = content[pc+3]
            if get(content, p1, imm1) == get(content, p2, imm2):
                content[p3] = 1
            else:
                content[p3] = 0
            pc += 4
        elif opcode == 99:
            # program exit
            break
        else:
            raise Exception("Unknown opcode " + str(opcode))
    return output

fh = open("07.txt", "r")
content = fh.read()
fh.close()
#content = "3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0"
#content = "3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0"
content = [int(x) for x in content.split(",")]

max_value = 0
for phase0 in [0,1,2,3,4]:
    value0 = run(content, [phase0, 0])
    for phase1 in [0,1,2,3,4]:
        if phase1 == phase0: continue
        value1 = run(content, [phase1, value0])
        for phase2 in [0,1,2,3,4]:
            if phase2 == phase1: continue
            if phase2 == phase0: continue
            value2 = run(content, [phase2, value1])
            for phase3 in [0,1,2,3,4]:
                if phase3 == phase2: continue
                if phase3 == phase1: continue
                if phase3 == phase0: continue
                value3 = run(content, [phase3, value2])
                for phase4 in [0,1,2,3,4]:
                    if phase4 == phase3: continue
                    if phase4 == phase2: continue
                    if phase4 == phase1: continue
                    if phase4 == phase0: continue
                    value4 = run(content, [phase4, value3])
                    print(value4, phase0, phase1, phase2, phase3, phase4)
                    max_value = max(max_value, value4)

print(max_value)

