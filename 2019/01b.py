#!/usr/bin/env python3
"""
Fuel required to launch a given module is based on its mass. Specifically, to find the fuel required for a module, take its mass, divide by three, round down, and subtract 2.

For example:

    For a mass of 12, divide by 3 and round down to get 4, then subtract 2 to get 2.
    For a mass of 14, dividing by 3 and rounding down still yields 4, so the fuel required is also 2.
    For a mass of 1969, the fuel required is 654.
    For a mass of 100756, the fuel required is 33583.

The Fuel Counter-Upper needs to know the total fuel requirement. To find it, individually calculate the fuel needed for the mass of each module (your puzzle input), then add together all the fuel values.

What is the sum of the fuel requirements for all of the modules on your spacecraft?
"""

def calc(num):
    result = 0
    while num > 0:
        num = num // 3 - 2
        if num > 0:
            print(num)
            result += num
    return result

sum = 0
fh = open("01.txt", "r")
line = fh.readline()
while line:
    line = line.strip()
    if line != '':
        num = int(line)
        sum += calc(num)
    line = fh.readline()

# print(calc(14))
# print(calc(1969))
# print(calc(14))
print(sum)