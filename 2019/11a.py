#!/usr/bin/env python3
"""
"""

from collections import defaultdict

def parse_op_code(input):
    opcode = input % 100
    input = input // 100
    mode1 = (input % 10)
    input = input // 10
    mode2 = (input % 10)
    input = input // 10
    mode3 = (input % 10)
    return (opcode, mode1, mode2, mode3)

assert(parse_op_code(1002) == (2, 0, 1, 0))

def get(memory, relative_base, value, mode):
    if mode == 0:
            # position mode
        if value < 0:
            raise Exception("Illegal address (0): " + str(value))
        return memory.get(value, 0)
    elif mode == 1:
        # immediate mode
        return value
    elif mode == 2:
        # relative mode
        if value+relative_base < 0:
            raise Exception("Illegal address (2): " + str(value+relative_base))
        return memory.get(value+relative_base, 0)
    else:
        raise Exception("Unknown mode " + str(mode))

def set(memory, relative_base, mode, address, value):
    if mode == 0:
        # position mode
        if address < 0:
            raise Exception("Illegal address (0): " + str(address))
        memory[address] = value
    elif mode == 1:
        # immediate mode
        raise Exception("Can't set in immediate mode")
    elif mode == 2:
        # relative mode
        if address+relative_base < 0:
            raise Exception("Illegal address (2): " + str(address+relative_base))
        memory[address+relative_base] = value
    else:
        raise Exception("Unknown mode " + str(mode))

DEBUG = True

def run(memory_array, relative_base):
    # (x,y) => color (0: black, 1: white)
    memory = {a: b for (a,b) in enumerate(memory_array)}
    panels = defaultdict(int)
    position = (0,0)
    direction = (0,-1) # point up
    pc = 0
    inputIndex = 0
    output = []
    while True:
        (opcode, mode1, mode2, mode3) = parse_op_code(memory[pc])
        if opcode == 1:
            # addition
            p1 = memory[pc+1]
            p2 = memory[pc+2]
            p3 = memory[pc+3]
            set(memory, relative_base, mode3, p3, get(memory, relative_base, p1, mode1) + get(memory, relative_base, p2, mode2))
            #if DEBUG: print("DEBUG: addition " + str((p1, p2, p3, memory[p3])))
            pc += 4
        elif opcode == 2:
            # multiplication
            p1 = memory[pc+1]
            p2 = memory[pc+2]
            p3 = memory[pc+3]
            set(memory, relative_base, mode3, p3, get(memory, relative_base, p1, mode1) * get(memory, relative_base, p2, mode2))
            pc += 4
        elif opcode == 3:
            p1 = memory[pc+1]
            input = panels[position]
            set(memory, relative_base, mode1, p1, input)
            pc += 2
        elif opcode == 4:
            p1 = memory[pc+1]
            output.append(get(memory, relative_base, p1, mode1))
            if len(output) == 2:
                # Paint panel
                if output[0] not in [0,1]:
                    raise Exception("Illegal paint " + str(output[0]))
                panels[position] = output[0]
                # Turn: 0: left; 1: right
                if output[1] == 0:
                    direction = (direction[1], -direction[0])
                elif output[1] == 1:
                    direction = (-direction[1], direction[0])
                else:
                    raise Exception("Illegal turn: " + str(output[1]))
                position = (position[0] + direction[0], position[1] + direction[1])
                output = []
            pc += 2
        elif opcode == 5:
            p1 = memory[pc+1]
            p2 = memory[pc+2]
            if get(memory, relative_base, p1, mode1) != 0:
                pc = get(memory, relative_base, p2, mode2)
            else:
                pc += 3
        elif opcode == 6:
            p1 = memory[pc+1]
            p2 = memory[pc+2]
            if get(memory, relative_base, p1, mode1) == 0:
                pc = get(memory, relative_base, p2, mode2)
            else:
                pc += 3
        elif opcode == 7:
            p1 = memory[pc+1]
            p2 = memory[pc+2]
            p3 = memory[pc+3]
            if get(memory, relative_base, p1, mode1) < get(memory, relative_base, p2, mode2):
                set(memory, relative_base, mode3, p3, 1)
            else:
                set(memory, relative_base, mode3, p3, 0)
            pc += 4
        elif opcode == 8:
            p1 = memory[pc+1]
            p2 = memory[pc+2]
            p3 = memory[pc+3]
            if get(memory, relative_base, p1, mode1) == get(memory, relative_base, p2, mode2):
                set(memory, relative_base, mode3, p3, 1)
            else:
                set(memory, relative_base, mode3, p3, 0)
            pc += 4
        elif opcode == 9:
            p1 = memory[pc+1]
            relative_base += get(memory, relative_base, p1, mode1)
            #print("adjust relative_base: " + str((p1, mode1, relative_base)))
            pc += 2
        elif opcode == 99:
            # program exit
            break
        else:
            raise Exception("Unknown opcode " + str(opcode))
    return panels

fh = open("11.txt", "r")
content = fh.read()
fh.close()
memory_array = [int(x) for x in content.split(",")]
output = run(memory_array, 0)
print(len(output))

#whites = [pos for (pos, color) in output.items() if color == 1]
#xs = [x for (x,y) in whites]
#ys = [y for (x,y) in whites]
#min_x = min(xs)
#max_x = max(xs)
#min_y = min(ys)
#max_y = max(ys)
#for y in range(max_y, min_y, -1):
#    line = []
#    for x in range(min_x, max_x):
#        line.append("#" if output[(x,y)] == 0 else ".")
#    print ("".join(line))


