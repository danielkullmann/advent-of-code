#!/usr/bin/env python3
"""
"""

from collections import defaultdict

def parse_op_code(input):
    opcode = input % 100
    input = input // 100
    mode1 = (input % 10)
    input = input // 10
    mode2 = (input % 10)
    input = input // 10
    mode3 = (input % 10)
    return (opcode, mode1, mode2, mode3)

assert(parse_op_code(1002) == (2, 0, 1, 0))

def get(memory, relative_base, value, mode):
    if mode == 0:
            # position mode
        if value < 0:
            raise Exception("Illegal address (0): " + str(value))
        return memory.get(value, 0)
    elif mode == 1:
        # immediate mode
        return value
    elif mode == 2:
        # relative mode
        if value+relative_base < 0:
            raise Exception("Illegal address (2): " + str(value+relative_base))
        return memory.get(value+relative_base, 0)
    else:
        raise Exception("Unknown mode " + str(mode))

def set_value(memory, relative_base, mode, address, value):
    if mode == 0:
        # position mode
        if address < 0:
            raise Exception("Illegal address (0): " + str(address))
        memory[address] = value
    elif mode == 1:
        # immediate mode
        raise Exception("Can't set_value in immediate mode")
    elif mode == 2:
        # relative mode
        if address+relative_base < 0:
            raise Exception("Illegal address (2): " + str(address+relative_base))
        memory[address+relative_base] = value
    else:
        raise Exception("Unknown mode " + str(mode))

def get_bounds(screen):
    cells = [pos for (pos, color) in screen.items()]
    xs = [x for (x,y) in cells] + [position[0]]
    ys = [y for (x,y) in cells] + [position[1]]
    return (min(xs), max(xs), min(ys), max(ys))

def show_screen(position, screen, trace, visited, target):
    (min_x, max_x, min_y, max_y) = get_bounds(screen)
    for y in range(min_y, max_y+1):
        line = []
        for x in range(min_x, max_x+1):
            char = "?"
            if (x,y) == position:
                char = "R"
            elif screen[(x,y)] == 1:
                char = "#"
            elif (x,y) == (0,0):
                char = "x"
            elif (x,y) in trace:
                index = trace.index((x,y)) % 10
                char = str(index)
            elif (x,y) == target:
                char = "X"
            elif (x,y) in visited:
                char = "."
            line.append(char)
        print ("".join(line))

pc = 0
relative_base = 0
# (x,y) => sell (0: empty, 1: wall)
screen = defaultdict(int)
memory = None

def run(robot_command):
    global pc, relative_base, screen, memory
    while True:
        (opcode, mode1, mode2, mode3) = parse_op_code(memory[pc])
        if opcode == 1:
            # addition
            p1 = memory[pc+1]
            p2 = memory[pc+2]
            p3 = memory[pc+3]
            set_value(memory, relative_base, mode3, p3, get(memory, relative_base, p1, mode1) + get(memory, relative_base, p2, mode2))
            pc += 4
        elif opcode == 2:
            # multiplication
            p1 = memory[pc+1]
            p2 = memory[pc+2]
            p3 = memory[pc+3]
            set_value(memory, relative_base, mode3, p3, get(memory, relative_base, p1, mode1) * get(memory, relative_base, p2, mode2))
            pc += 4
        elif opcode == 3:
            p1 = memory[pc+1]
            set_value(memory, relative_base, mode1, p1, robot_command)
            pc += 2
        elif opcode == 4:
            p1 = memory[pc+1]
            pc += 2
            return get(memory, relative_base, p1, mode1)
        elif opcode == 5:
            p1 = memory[pc+1]
            p2 = memory[pc+2]
            if get(memory, relative_base, p1, mode1) != 0:
                pc = get(memory, relative_base, p2, mode2)
            else:
                pc += 3
        elif opcode == 6:
            p1 = memory[pc+1]
            p2 = memory[pc+2]
            if get(memory, relative_base, p1, mode1) == 0:
                pc = get(memory, relative_base, p2, mode2)
            else:
                pc += 3
        elif opcode == 7:
            p1 = memory[pc+1]
            p2 = memory[pc+2]
            p3 = memory[pc+3]
            if get(memory, relative_base, p1, mode1) < get(memory, relative_base, p2, mode2):
                set_value(memory, relative_base, mode3, p3, 1)
            else:
                set_value(memory, relative_base, mode3, p3, 0)
            pc += 4
        elif opcode == 8:
            p1 = memory[pc+1]
            p2 = memory[pc+2]
            p3 = memory[pc+3]
            if get(memory, relative_base, p1, mode1) == get(memory, relative_base, p2, mode2):
                set_value(memory, relative_base, mode3, p3, 1)
            else:
                set_value(memory, relative_base, mode3, p3, 0)
            pc += 4
        elif opcode == 9:
            p1 = memory[pc+1]
            relative_base += get(memory, relative_base, p1, mode1)
            #print("adjust relative_base: " + str((p1, mode1, relative_base)))
            pc += 2
        elif opcode == 99:
            # program exit
            break
        else:
            raise Exception("Unknown opcode " + str(opcode))
    return None


fh = open("15.txt", "r")
content = fh.read()
fh.close()
memory_array = [int(x) for x in content.split(",")]
memory = {a: b for (a,b) in enumerate(memory_array)}

back_command_stack = []
position = (0,0)
direction_map = {
    1: (0, -1), # North
    2: (0, 1),  # South
    3: (-1,0),  # West
    4: (1,0),   # East
}
reverse_map = {1: 2, 2: 1, 3: 4, 4: 3}
directions = list(direction_map.keys())

HIT_WALL = 0
MOVED = 1
MOVED_TARGET = 2

target = None
bounds = None

# Each element contains:
# - back_direction (so that the robot can move back into the previous direction
# - position that the robot should be in when coming back here
# - directions_to_go: list of directions that still need to be checked out
stack = [(None, position, directions[:])]

step = 0
visited = set(position)
while len(stack) > 0:
    step += 1
    #if step > 50: break
    (back_direction, old_position, remaining_directions) = stack[-1]
    if len(remaining_directions) > 0:
        robot_command = remaining_directions.pop(0)
        #print("##### ", step)
        #print("Pos: ", position, "; Command: ", robot_command, "; dx/dy: ", direction_map[robot_command])
        #print("")
        #show_screen(position, screen, [e[1] for e in stack], visited, target)
        #print("")

        dd = direction_map[robot_command]
        new_position = (position[0] + dd[0], position[1] + dd[1])
        may_move = True
        if target != None:
            # Target is found, so I want to restrict the movements of the robot to the known universe
            (min_x, max_x, min_y, max_y) = bounds
            (x, y) = new_position
            if x < min_x or x > max_x:
                may_move = False
            if y < min_y or y > max_y:
                may_move = False
        if may_move and new_position not in visited:
            # This means this square has not been visited yet, so I try to visit it
            output = run(robot_command)
            #print(">>", str(output), "to", new_position)
            visited.add(new_position)

            if output == HIT_WALL:
                # don't change position
                screen[new_position] = 1
                pass
            elif output == MOVED:
                position = new_position
                screen[new_position] = 0
                stack.append((reverse_map[robot_command], position, directions[:]))
                #print("Step into", stack[-1])
            elif output == MOVED_TARGET:
                position = new_position
                screen[new_position] = 0
                target = position
                bounds = get_bounds(screen)
                stack.append((reverse_map[robot_command], position, directions[:]))
                print("Target:",target)
                #break
            else:
                raise Exception("Unknown output from robot: " + str(output))
        else:
            pass
            #print("Robot was already at", new_position)

    if len(stack[-1][2]) == 0:
        # remove top stack element
        stack.pop()
        # move robot back to last position
        output2 = run(back_direction)
        if output2 == None:
            break
        dd = direction_map[back_direction]
        new_position = (position[0] + dd[0], position[1] + dd[1])
        #print("Back to", stack[-1], "from", position, "via", dd, "to", new_position)
        old_position = stack[-1][1]
        if output2 != MOVED: raise Exception("Robot didn't move back: " + str(output2))
        if new_position != old_position: raise Exception("Robot didn't get into correct position " + str(new_position) + " " + str(old_position))
        position = new_position

show_screen(position, screen, [], visited, target)
print(len(screen))
print("Num Steps:", abs(target[0]) + abs(target[1]))

# Now find number of steps between (0,0) and target
min_number_of_steps = None
# Position, number of steps so far, remaining_directions, visited
stack = [((0,0), 0, directions[:], set((0,0)))]

while len(stack) > 0:
    (position, num_steps, remaining_directions, visited) = stack[-1]
    if position == target:
        if min_number_of_steps is None or num_steps < min_number_of_steps:
            min_number_of_steps = num_steps
    if len(remaining_directions) > 0:
        direction = remaining_directions.pop(0)
        dd = direction_map[direction]
        new_position = (position[0] + dd[0], position[1] + dd[1])
        if new_position not in visited and new_position in screen and screen[new_position] == 0:
            new_visited = visited.copy()
            new_visited.add(new_position)
            stack.append((new_position, num_steps+1, directions[:], new_visited))
    else:
        stack.pop()

print(min_number_of_steps)
