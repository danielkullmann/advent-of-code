#!/usr/bin/env python3

from collections import defaultdict

fh = open("14.txt")
lines = [s.strip() for s in fh.readlines()]
fh.close()
reactions = []
for line in lines:
    (before, after) = line.split(" => ")
    before = [e.split(" ") for e in before.split(", ")]
    after = after.split(" ")
    befores = []
    for item in before:
        befores.append((int(item[0]), item[1]))
    reactions.append((befores, int(after[0]), after[1]))

#for reaction in reactions:
#    print(str(reaction[0]) + "  => " + str(reaction[1]) + " " + reaction[2])

wanteds = defaultdict(int)
extras = defaultdict(int)
num_ores = 1000000000000
num_fuel = 0
while num_ores >= 103:
    wanteds["FUEL"] = 1
    while len(wanteds) > 0:
        new_wanteds = defaultdict(int)
        #print("####################")
        for (wanted, num_wanted) in wanteds.items():
            if wanted == "ORE":
                num_ores -= num_wanted
            else:
                if wanted in extras:
                    extra_has = extras[wanted]
                    if extra_has > num_wanted:
                        extras[wanted] -= num_wanted
                        continue # all wanted are available in extras
                    else: # extra_has <= num_wanted
                        num_wanted -= extra_has
                        extras[wanted] = 0
                        if num_wanted == 0:
                            continue
                wanted_reactions = [(reaction[0], reaction[1]) for reaction in reactions if reaction[2] == wanted]
                if len(wanted_reactions) != 1:
                    raise Exception("More than 1 rule for " + wanted)
                wanted_reaction = wanted_reactions[0]
                num_produced = wanted_reaction[1]
                multiplicator = 1
                if num_wanted > num_produced:
                    multiplicator = (num_wanted+num_produced-1) // num_produced
                num_produced *= multiplicator
                extras[wanted] += (num_produced-num_wanted)

                #print(str(num_wanted) + " " + wanted + " <= " + str(multiplicator) + " " + str(wanted_reaction))
                #print("  : " + wanted + " " + str(num_produced-num_wanted))
                for (num_prereq, prereq) in wanted_reaction[0]:
                    produced = num_prereq * multiplicator
                    new_wanteds[prereq] += produced
                    #print("  => " + prereq + " " + str(produced))
        wanteds = new_wanteds
    num_fuel += 1
    if num_fuel % 1000 == 0:
        print(num_fuel, num_ores)
print(num_fuel-1)


