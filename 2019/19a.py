#!/usr/bin/env python3
"""
"""

from collections import defaultdict
import sys

def parse_op_code(input):
    opcode = input % 100
    input = input // 100
    mode1 = (input % 10)
    input = input // 10
    mode2 = (input % 10)
    input = input // 10
    mode3 = (input % 10)
    return (opcode, mode1, mode2, mode3)

assert(parse_op_code(1002) == (2, 0, 1, 0))
assert(parse_op_code(21002) == (2, 0, 1, 2))

def get_value(memory, relative_base, value, mode):
    if mode == 0:
            # position mode
        if value < 0:
            raise Exception("Illegal address (0): " + str(value))
        return memory.get(value, 0)
    elif mode == 1:
        # immediate mode
        return value
    elif mode == 2:
        # relative mode
        if value+relative_base < 0:
            raise Exception("Illegal address (2): " + str(value+relative_base))
        return memory.get(value+relative_base, 0)
    else:
        raise Exception("Unknown mode " + str(mode))

def set_value(memory, relative_base, mode, address, value):
    if mode == 0:
        # position mode
        if address < 0:
            raise Exception("Illegal address (0): " + str(address))
        memory[address] = value
    elif mode == 1:
        # immediate mode
        raise Exception("Can't set_value in immediate mode")
    elif mode == 2:
        # relative mode
        if address+relative_base < 0:
            raise Exception("Illegal address (2): " + str(address+relative_base))
        memory[address+relative_base] = value
    else:
        raise Exception("Unknown mode " + str(mode))

def get_bounds(screen, position):
    cells = [pos for (pos, color) in screen.items()]
    xs = [x for (x,y) in cells]
    ys = [y for (x,y) in cells]
    if position is not None:
        xs += [position[0]]
        ys += [position[1]]
    return (min(xs), max(xs), min(ys), max(ys))

def show_screen(position, screen, trace, visited, target):
    (min_x, max_x, min_y, max_y) = get_bounds(screen, position)
    for y in range(min_y, max_y+1):
        line = []
        for x in range(min_x, max_x+1):
            char = "?"
            if (x,y) == position:
                char = "R"
            elif (x,y) == target:
                char = "X"
            elif (x,y) == (0,0):
                char = "x"
            elif (x,y) in trace:
                index = trace.index((x,y)) % 10
                char = str(index)
            elif screen[(x,y)] == 1:
                char = "#"
            elif screen[(x,y)] == 2:
                char = "."
            elif (x,y) in visited:
                char = "O"
            line.append(char)
        print ("".join(line))

def run(memory, inputs):
    pc = 0
    relative_base = 0
    output= []
    inputIndex = 0
    while True:
        #print("PC:", pc, relative_base)
        (opcode, mode1, mode2, mode3) = parse_op_code(memory[pc])
        #print("OPCODE:", (opcode, mode1, mode2, mode3))
        if opcode == 1:
            # addition
            p1 = memory[pc+1]
            p2 = memory[pc+2]
            p3 = memory[pc+3]
            set_value(memory, relative_base, mode3, p3, get_value(memory, relative_base, p1, mode1) + get_value(memory, relative_base, p2, mode2))
            pc += 4
        elif opcode == 2:
            # multiplication
            p1 = memory[pc+1]
            p2 = memory[pc+2]
            p3 = memory[pc+3]
            set_value(memory, relative_base, mode3, p3, get_value(memory, relative_base, p1, mode1) * get_value(memory, relative_base, p2, mode2))
            pc += 4
        elif opcode == 3:
            # input
            p1 = memory[pc+1]
            set_value(memory, relative_base, mode1, p1, inputs[inputIndex])
            inputIndex += 1
            pc += 2
        elif opcode == 4:
            # output
            p1 = memory[pc+1]
            pc += 2
            output.append(get_value(memory, relative_base, p1, mode1))
        elif opcode == 5:
            # jnz
            p1 = memory[pc+1]
            p2 = memory[pc+2]
            if get_value(memory, relative_base, p1, mode1) != 0:
                pc = get_value(memory, relative_base, p2, mode2)
            else:
                pc += 3
        elif opcode == 6:
            # jz
            p1 = memory[pc+1]
            p2 = memory[pc+2]
            #print("JZ", p1, p2, get_value(memory, relative_base, p1, mode1))
            if get_value(memory, relative_base, p1, mode1) == 0:
                pc = get_value(memory, relative_base, p2, mode2)
            else:
                pc += 3
        elif opcode == 7:
            # jlt
            p1 = memory[pc+1]
            p2 = memory[pc+2]
            p3 = memory[pc+3]
            if get_value(memory, relative_base, p1, mode1) < get_value(memory, relative_base, p2, mode2):
                set_value(memory, relative_base, mode3, p3, 1)
            else:
                set_value(memory, relative_base, mode3, p3, 0)
            pc += 4
        elif opcode == 8:
            # jeq
            p1 = memory[pc+1]
            p2 = memory[pc+2]
            p3 = memory[pc+3]
            if get_value(memory, relative_base, p1, mode1) == get_value(memory, relative_base, p2, mode2):
                set_value(memory, relative_base, mode3, p3, 1)
            else:
                set_value(memory, relative_base, mode3, p3, 0)
            pc += 4
        elif opcode == 9:
            # adjust relative base
            p1 = memory[pc+1]
            relative_base += get_value(memory, relative_base, p1, mode1)
            #print("adjust relative_base: " + str((p1, mode1, relative_base)))
            pc += 2
        elif opcode == 99:
            # program exit
            break
        else:
            raise Exception("Unknown opcode " + str(opcode))
    return output


fh = open("19.txt", "r")
content = fh.read()
fh.close()
memory_array = [int(x) for x in content.split(",")]
memory = {a: b for (a,b) in enumerate(memory_array)}

tractor_beam_area = 0
for y in range(0, 50):
    for x in range (0, 50):
        tractor_beam = run(memory, [x, y])[0]
        tractor_beam_area += 1 if tractor_beam == 46 else 0
        #print("#" if tractor_beam == 1 else ".", end="")
        print(chr(tractor_beam), end="")
        sys.stdout.flush()
    print("")

print(tractor_beam_area)