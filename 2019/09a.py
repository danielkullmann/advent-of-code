#!/usr/bin/env python3
"""
"""

def parse_op_code(input):
    opcode = input % 100
    input = input // 100
    mode1 = (input % 10)
    input = input // 10
    mode2 = (input % 10)
    input = input // 10
    mode3 = (input % 10)
    return (opcode, mode1, mode2, mode3)

assert(parse_op_code(1002) == (2, 0, 1, 0))

def get(memory, relative_base, value, mode):
    if mode == 0:
            # position mode
        if value < 0:
            raise Exception("Illegal address (0): " + str(value))
        return memory.get(value, 0)
    elif mode == 1:
        # immediate mode
        return value
    elif mode == 2:
        # relative mode
        if value+relative_base < 0:
            raise Exception("Illegal address (2): " + str(value+relative_base))
        return memory.get(value+relative_base, 0)
    else:
        raise Exception("Unknown mode " + str(mode))

def set(memory, relative_base, mode, address, value):
    if mode == 0:
        # position mode
        if address < 0:
            raise Exception("Illegal address (0): " + str(address))
        memory[address] = value
    elif mode == 1:
        # immediate mode
        raise Exception("Can't set in immediate mode")
    elif mode == 2:
        # relative mode
        if address+relative_base < 0:
            raise Exception("Illegal address (2): " + str(address+relative_base))
        memory[address+relative_base] = value
    else:
        raise Exception("Unknown mode " + str(mode))

DEBUG = True

def run(memory, relative_base, input):
    pc = 0
    inputIndex = 0
    output = []
    while True:
        (opcode, mode1, mode2, mode3) = parse_op_code(memory[pc])
        if opcode == 1:
            # addition
            p1 = memory[pc+1]
            p2 = memory[pc+2]
            p3 = memory[pc+3]
            set(memory, relative_base, mode3, p3, get(memory, relative_base, p1, mode1) + get(memory, relative_base, p2, mode2))
            if DEBUG: print("DEBUG: addition " + str((p1, p2, p3, memory[p3])))
            pc += 4
        elif opcode == 2:
            # multiplication
            p1 = memory[pc+1]
            p2 = memory[pc+2]
            p3 = memory[pc+3]
            set(memory, relative_base, mode3, p3, get(memory, relative_base, p1, mode1) * get(memory, relative_base, p2, mode2))
            pc += 4
        elif opcode == 3:
            p1 = memory[pc+1]
            set(memory, relative_base, mode1, p1, input[inputIndex])
            inputIndex += 1
            pc += 2
        elif opcode == 4:
            p1 = memory[pc+1]
            output.append(get(memory, relative_base, p1, mode1))
            pc += 2
        elif opcode == 5:
            p1 = memory[pc+1]
            p2 = memory[pc+2]
            if get(memory, relative_base, p1, mode1) != 0:
                pc = get(memory, relative_base, p2, mode2)
            else:
                pc += 3
        elif opcode == 6:
            p1 = memory[pc+1]
            p2 = memory[pc+2]
            if get(memory, relative_base, p1, mode1) == 0:
                pc = get(memory, relative_base, p2, mode2)
            else:
                pc += 3
        elif opcode == 7:
            p1 = memory[pc+1]
            p2 = memory[pc+2]
            p3 = memory[pc+3]
            if get(memory, relative_base, p1, mode1) < get(memory, relative_base, p2, mode2):
                set(memory, relative_base, mode3, p3, 1)
            else:
                set(memory, relative_base, mode3, p3, 0)
            pc += 4
        elif opcode == 8:
            p1 = memory[pc+1]
            p2 = memory[pc+2]
            p3 = memory[pc+3]
            if get(memory, relative_base, p1, mode1) == get(memory, relative_base, p2, mode2):
                set(memory, relative_base, mode3, p3, 1)
            else:
                set(memory, relative_base, mode3, p3, 0)
            pc += 4
        elif opcode == 9:
            p1 = memory[pc+1]
            relative_base += get(memory, relative_base, p1, mode1)
            #print("adjust relative_base: " + str((p1, mode1, relative_base)))
            pc += 2
        elif opcode == 99:
            # program exit
            break
        else:
            raise Exception("Unknown opcode " + str(opcode))
    #print(output)
    return output

def doit(content):
    memory_array = [int(x) for x in content.split(",")]
    memory = {a: b for (a,b) in enumerate(memory_array)}
    output = run(memory, 0, [1])
    return output

content = "109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99"
assert(",".join(map(str,doit(content))) == content)

content = "1102,34915192,34915192,7,4,7,99,0"
assert(len(str(doit(content)[0])) == 16)

content = "104,1125899906842624,99"
assert(str(doit(content)[0]) == content.split(",")[1])

content = "4,1125899906842624,99"
assert(doit(content)[0] == 0)

content = "204,1125899906842624,99"
assert(doit(content)[0] == 0)

fh = open("09.txt", "r")
content = fh.read()
fh.close()

print(doit(content))

