#!/usr/bin/env python3
"""
"""

sum = 0
fh = open("02.txt", "r")
content = fh.read()
fh.close()

content = [int(x) for x in content.split(",")]
content[1] = 12
content[2] = 2
pc = 0
while True:
    opcode = content[pc]
    if opcode == 1:
        # addition
        p1 = content[pc+1]
        p2 = content[pc+2]
        p3 = content[pc+3]
        content[p3] = content[p1] + content[p2]
        pc += 4
    elif opcode == 2:
        # multiplication
        p1 = content[pc+1]
        p2 = content[pc+2]
        p3 = content[pc+3]
        content[p3] = content[p1] * content[p2]
        pc += 4
    elif opcode == 99:
        # program exit
        break
    else:
        raise Exception("Unknown opcode " + str(opcode))

print(content)