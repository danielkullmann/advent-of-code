import heapq
import sys

fh = open("18.txt")
labyrinth = [line.strip() for line in fh.readlines()]
fh.close()

DOORS = list(map(chr, range(ord("A"), ord("Z")+1)))
KEYS = list(map(chr, range(ord("a"), ord("z")+1)))

def find_start(labyrinth):
    for y in range(len(labyrinth)):
        row = labyrinth[y]
        try:
            return (y, row.index("@"))
        except Exception:
            pass

def get_neighbors(position, labyrinth):
    (y,x) = position
    potential_neighbors = [
        (y-1, x), (y+1, x), (y, x-1), (y, x+1)
    ]
    result = []
    for (ny, nx) in potential_neighbors:
        if ny < 0 or nx < 0 or ny >= len(labyrinth) or nx > len(labyrinth[ny]):
            continue
        if labyrinth[ny][nx] == "#":
            continue
        result.append((ny, nx))
    return result

def get_distance_info(start_cell, start_position, labyrinth):
    distances = {}
    doors_in_the_way = set()
    queue = [(start_position, 0, set())]
    visited = set()
    while len(queue) > 0:
        (position, distance_from_start, doors_in_the_way) = queue.pop()
        if position in visited:
            continue
        visited.add(position)
        cell = labyrinth[position[0]][position[1]]
        if cell in KEYS:
            distances[(start_cell, cell)] = (distance_from_start, doors_in_the_way)
            distances[(cell, start_cell)] = (distance_from_start, doors_in_the_way)
        elif cell in DOORS:
            doors_in_the_way = doors_in_the_way.copy()
            doors_in_the_way.add(cell.lower())

        neighbors = get_neighbors(position, labyrinth)
        for neighbor in neighbors:
            queue.append((neighbor, distance_from_start+1, doors_in_the_way))
    return distances

def find_doors_and_keys(start, labyrinth):
    # (p1, p2) => (distance, doors_in_the_way)
    distances = {}
    # (p1) => position
    locations = {}
    locations["@"] = start
    visited = set()
    # (position, distance_from_start, doors_in_the_way)
    queue = [(start, 0, set())]
    while len(queue) > 0:
        (position, distance_from_start, doors_in_the_way) = queue.pop()
        if position in visited:
            continue
        visited.add(position)
        cell = labyrinth[position[0]][position[1]]
        if cell in DOORS:
            #locations[cell] = position
            doors_in_the_way = doors_in_the_way.copy()
            doors_in_the_way.add(cell.lower())
        elif cell in KEYS:
            locations[cell] = position
            distances[("@", cell)] = (distance_from_start, doors_in_the_way)
            distances[(cell, "@")] = (distance_from_start, doors_in_the_way)

        neighbors = get_neighbors(position, labyrinth)
        for neighbor in neighbors:
            queue.append((neighbor, distance_from_start+1, doors_in_the_way))
    
    for c in locations.keys():
        p1 = locations[c]
        add_distances = get_distance_info(c, p1, labyrinth)
        distances.update(add_distances)

    return (distances, locations.keys() - set(["@"]))
        

# labyrinth = """########################
# #f.D.E.e.C.b.A.@.a.B.c.#
# ######################.#
# #d.....................#
# ########################""".split("\n")

start = find_start(labyrinth)
#for row in labyrinth: print(row)
#print("Start", start)
(distances, found_keys) = find_doors_and_keys(start, labyrinth)
#for (a,b) in distances.items():
#    print(a,b)
#print()
#sys.stdout.flush()

# Now we need to find a path in the map that picks up all keys

# (cell, distance_so_far, keys_collected)
#heap = [(0, 0, "@", set())]
heap = []
heapq.heappush(heap, (0, 0, "@", set()))

visited = set()
min_distance = None
steps = 0
shortcuts = 0
while len(heap) > 0:
    (_, distance_so_far, cell, keys_collected) = heapq.heappop(heap)
    #(_, distance_so_far, cell, keys_collected) = heap.pop()
    steps += 1
    if steps % 1_000_000 == 0:
        print("#############################")
        print(steps, shortcuts, min_distance)
        for item in heapq.nsmallest(50, heap):
            print(item)
        sys.stdout.flush()
    visited.add(cell)
    if cell != "@":
        keys_collected = keys_collected.copy()
        keys_collected.add(cell)
        if len(keys_collected) == len(found_keys):
            if min_distance is None or distance_so_far < min_distance:
                min_distance = distance_so_far
                print("\nFound", min_distance)
                sys.stdout.flush()
    neighbors = [
        (distance, other_cell, doors_in_the_way) 
        for ((this_cell, other_cell), (distance, doors_in_the_way)) 
        in distances.items()
        if cell == this_cell
        and other_cell != "@"
        and other_cell not in keys_collected
        and doors_in_the_way - keys_collected == set()
    ]
    #neighbors = sorted(neighbors, reverse=True)
    #print (distance_so_far, cell, path, neighbors, keys_collected)
    for (add_distance, other_cell, _) in neighbors:
        if min_distance is not None and distance_so_far+add_distance > min_distance:
            # We won't be able to make a better path
            shortcuts += 1
            continue
        heapq.heappush(
            heap, 
            (-len(keys_collected), distance_so_far+add_distance, other_cell, keys_collected)
        )
        #heap.append((-len(keys_collected), distance_so_far+add_distance, other_cell, keys_collected))

print(min_distance)