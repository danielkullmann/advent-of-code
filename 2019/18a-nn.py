import random
import sys

fh = open("18.txt")
labyrinth = [line.strip() for line in fh.readlines()]
fh.close()

DOORS = list(map(chr, range(ord("A"), ord("Z")+1)))
KEYS = list(map(chr, range(ord("a"), ord("z")+1)))

def find_start(labyrinth):
    for y in range(len(labyrinth)):
        row = labyrinth[y]
        try:
            return (y, row.index("@"))
        except Exception:
            pass

def get_neighbors(position, labyrinth):
    (y,x) = position
    potential_neighbors = [
        (y-1, x), (y+1, x), (y, x-1), (y, x+1)
    ]
    result = []
    for (ny, nx) in potential_neighbors:
        if ny < 0 or nx < 0 or ny >= len(labyrinth) or nx > len(labyrinth[ny]):
            continue
        if labyrinth[ny][nx] == "#":
            continue
        result.append((ny, nx))
    return result

def get_distance_info(start_cell, start_position, labyrinth):
    distances = {}
    doors_in_the_way = set()
    queue = [(start_position, 0, set())]
    visited = set()
    while len(queue) > 0:
        (position, distance_from_start, doors_in_the_way) = queue.pop()
        if position in visited:
            continue
        visited.add(position)
        cell = labyrinth[position[0]][position[1]]
        if cell in KEYS:
            distances[(start_cell, cell)] = (distance_from_start, doors_in_the_way)
            distances[(cell, start_cell)] = (distance_from_start, doors_in_the_way)
        elif cell in DOORS:
            doors_in_the_way = doors_in_the_way.copy()
            doors_in_the_way.add(cell.lower())

        neighbors = get_neighbors(position, labyrinth)
        for neighbor in neighbors:
            queue.append((neighbor, distance_from_start+1, doors_in_the_way))
    return distances

def find_doors_and_keys(start, labyrinth):
    # (p1, p2) => (distance, doors_in_the_way)
    distances = {}
    # (p1) => position
    locations = {}
    locations["@"] = start
    visited = set()
    # (position, distance_from_start, doors_in_the_way)
    queue = [(start, 0, set())]
    while len(queue) > 0:
        (position, distance_from_start, doors_in_the_way) = queue.pop()
        if position in visited:
            continue
        visited.add(position)
        cell = labyrinth[position[0]][position[1]]
        if cell in DOORS:
            #locations[cell] = position
            doors_in_the_way = doors_in_the_way.copy()
            doors_in_the_way.add(cell.lower())
        elif cell in KEYS:
            locations[cell] = position
            distances[("@", cell)] = (distance_from_start, doors_in_the_way)
            distances[(cell, "@")] = (distance_from_start, doors_in_the_way)

        neighbors = get_neighbors(position, labyrinth)
        for neighbor in neighbors:
            queue.append((neighbor, distance_from_start+1, doors_in_the_way))

    for c in locations.keys():
        p1 = locations[c]
        add_distances = get_distance_info(c, p1, labyrinth)
        distances.update(add_distances)

    return (distances, locations.keys() - set(["@"]))


# labyrinth = """########################
# #f.D.E.e.C.b.A.@.a.B.c.#
# ######################.#
# #d.....................#
# ########################""".split("\n")

start = find_start(labyrinth)
#for row in labyrinth: print(row)
#print("Start", start)
(distances, found_keys) = find_doors_and_keys(start, labyrinth)
for (a,b) in distances.items():
    print(a,b)
print()
sys.stdout.flush()

# Now we need to find a path in the map that picks up all keys

# Nearest neighbor:
nn_distance = 0
node = "@"
to_visit = found_keys
keys_collected = []
while len(keys_collected) != len(to_visit):
    neighbors = [
        (distance, other_cell, doors_in_the_way)
        for ((this_cell, other_cell), (distance, doors_in_the_way))
        in distances.items()
        if node == this_cell
        and other_cell != "@"
        and other_cell not in keys_collected
        and doors_in_the_way - set(keys_collected) == set()
    ]
    neighbors = sorted(neighbors)
    (distance, node, _) = neighbors[0]
    keys_collected.append(node)
    nn_distance += distance

def length(keys):
    last_key = "@"
    result = 0
    for key in keys:
        (distance, _) = distances[(last_key, key)]
        result += distance
        last_key = key
    return result

def is_legal(keys):
    last_key = "@"
    collected = set()
    for key in keys:
        (_, deps) = distances[(last_key, key)]
        if deps - collected != set():
            return False
        collected.add(key)
        last_key = key
    return True

def mutate(keys):
    mutation_type = random.randrange(2)
    if mutation_type == 0:
        # Swap two indices
        keys = keys[:]
        i1 = random.randrange(len(keys))
        i2 = random.randrange(len(keys))
        temp = keys[i1]
        keys[i1] = keys[i2]
        keys[i2] = temp
        pass
    elif mutation_type == 1:
        # Cut and exchange
        idx = random.randrange(2, len(keys)-2)
        keys = keys[idx:] + keys[:idx]
        pass
    else:
        raise Exception("Can't happen (randrange)")
    return keys



print(nn_distance, is_legal(keys_collected), length(keys_collected), keys_collected)
print(is_legal(mutate(keys_collected)))
mutations = [(length(keys_collected), is_legal(keys_collected), keys_collected)]
MAX_GENE_POOL = 100
print(mutations[0])
for _ in range(100000):
    mutations = sorted(mutations, reverse=True)
    if len(mutations) > MAX_GENE_POOL*3//4:
        mutations = mutations[:MAX_GENE_POOL*3//4]
    while len(mutations) < MAX_GENE_POOL:
        pass
