
#!/usr/bin/env python3
"""
"""

from collections import defaultdict

def parse_op_code(input):
    opcode = input % 100
    input = input // 100
    mode1 = (input % 10)
    input = input // 10
    mode2 = (input % 10)
    input = input // 10
    mode3 = (input % 10)
    return (opcode, mode1, mode2, mode3)

assert(parse_op_code(1002) == (2, 0, 1, 0))

def get(memory, relative_base, value, mode):
    if mode == 0:
            # position mode
        if value < 0:
            raise Exception("Illegal address (0): " + str(value))
        return memory.get(value, 0)
    elif mode == 1:
        # immediate mode
        return value
    elif mode == 2:
        # relative mode
        if value+relative_base < 0:
            raise Exception("Illegal address (2): " + str(value+relative_base))
        return memory.get(value+relative_base, 0)
    else:
        raise Exception("Unknown mode " + str(mode))

def set(memory, relative_base, mode, address, value):
    if mode == 0:
        # position mode
        if address < 0:
            raise Exception("Illegal address (0): " + str(address))
        memory[address] = value
    elif mode == 1:
        # immediate mode
        raise Exception("Can't set in immediate mode")
    elif mode == 2:
        # relative mode
        if address+relative_base < 0:
            raise Exception("Illegal address (2): " + str(address+relative_base))
        memory[address+relative_base] = value
    else:
        raise Exception("Unknown mode " + str(mode))

DEBUG = True

def run(memory_array, relative_base):
    # (x,y) => color (0: black, 1: white)
    memory = {a: b for (a,b) in enumerate(memory_array)}
    screen = defaultdict(int)
    pc = 0
    output = []
    while True:
        (opcode, mode1, mode2, mode3) = parse_op_code(memory[pc])
        if opcode == 1:
            # addition
            p1 = memory[pc+1]
            p2 = memory[pc+2]
            p3 = memory[pc+3]
            set(memory, relative_base, mode3, p3, get(memory, relative_base, p1, mode1) + get(memory, relative_base, p2, mode2))
            #if DEBUG: print("DEBUG: addition " + str((p1, p2, p3, memory[p3])))
            pc += 4
        elif opcode == 2:
            # multiplication
            p1 = memory[pc+1]
            p2 = memory[pc+2]
            p3 = memory[pc+3]
            set(memory, relative_base, mode3, p3, get(memory, relative_base, p1, mode1) * get(memory, relative_base, p2, mode2))
            pc += 4
        elif opcode == 3:
            raise Exception("Input")
        elif opcode == 4:
            p1 = memory[pc+1]
            output.append(get(memory, relative_base, p1, mode1))
            if len(output) == 3:
                (x,y,tile) = output
                screen[(x,y)] = tile
                output = []
            pc += 2
        elif opcode == 5:
            p1 = memory[pc+1]
            p2 = memory[pc+2]
            if get(memory, relative_base, p1, mode1) != 0:
                pc = get(memory, relative_base, p2, mode2)
            else:
                pc += 3
        elif opcode == 6:
            p1 = memory[pc+1]
            p2 = memory[pc+2]
            if get(memory, relative_base, p1, mode1) == 0:
                pc = get(memory, relative_base, p2, mode2)
            else:
                pc += 3
        elif opcode == 7:
            p1 = memory[pc+1]
            p2 = memory[pc+2]
            p3 = memory[pc+3]
            if get(memory, relative_base, p1, mode1) < get(memory, relative_base, p2, mode2):
                set(memory, relative_base, mode3, p3, 1)
            else:
                set(memory, relative_base, mode3, p3, 0)
            pc += 4
        elif opcode == 8:
            p1 = memory[pc+1]
            p2 = memory[pc+2]
            p3 = memory[pc+3]
            if get(memory, relative_base, p1, mode1) == get(memory, relative_base, p2, mode2):
                set(memory, relative_base, mode3, p3, 1)
            else:
                set(memory, relative_base, mode3, p3, 0)
            pc += 4
        elif opcode == 9:
            p1 = memory[pc+1]
            relative_base += get(memory, relative_base, p1, mode1)
            #print("adjust relative_base: " + str((p1, mode1, relative_base)))
            pc += 2
        elif opcode == 99:
            # program exit
            break
        else:
            raise Exception("Unknown opcode " + str(opcode))
    return screen

fh = open("13.txt", "r")
content = fh.read()
fh.close()
memory_array = [int(x) for x in content.split(",")]
output = run(memory_array, 0)

num_blocks = sum([1 for x in output.values() if x == 2])
print(num_blocks)

