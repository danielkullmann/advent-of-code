#!/usr/bin/env python3

fh = open("03.txt", "r")
wires1 = fh.readline().strip().split(",")
wires2 = fh.readline().strip().split(",")
fh.close()

def get_points(wires):
    result = set()
    pos = (0,0)
    for wire in wires:
        command = wire[0]
        length = int(wire[1:])
        if command == "D":
            for i in range(1,length+1):
                pos = (pos[0]+1, pos[1])
                result.add(pos)
        elif command == "U":
            for i in range(1,length+1):
                pos = (pos[0]-1, pos[1])
                result.add(pos)
        elif command == "L":
            for i in range(1,length+1):
                pos = (pos[0], pos[1]-1)
                result.add(pos)
        elif command == "R":
            for i in range(1,length+1):
                pos = (pos[0], pos[1]+1)
                result.add(pos)
        else:
            raise Exception("Unknown command " + command)
    return result

def calc(wires1, wires2):
    points1 = get_points(wires1)
    points2 = get_points(wires2)
    crossings = points1.intersection(points2)
    min_manhattan_distance = 99999999999
    for c in crossings:
        manhattan_distance = abs(c[0]) + abs(c[1])
        print(str(c) + ": " + str(manhattan_distance))
        min_manhattan_distance = min(min_manhattan_distance, manhattan_distance)
    print(min_manhattan_distance)


t1 = "R8,U5,L5,D3".split(",")
t2 = "U7,R6,D4,L4".split(",")
calc(t1, t2)


calc(wires1, wires2)
