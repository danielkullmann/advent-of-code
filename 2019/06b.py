"""
"""

fh = open("06.txt")
orbit_definitions = [line.split(")") for line in fh.read().strip().split("\n")]
fh.close()

# orbit_definitions = [
#     line.split(")") 
#     for line in "COM)B-B)C-C)D-D)E-E)F-B)G-G)H-D)I-E)J-J)K-K)L-K)YOU-I)SAN".split("-")
# ]

start = "YOU"
queue = [(start, 0)]
visited = set()
while len(queue) > 0:
    (node, depth) = queue.pop()
    if node == "SAN":
        print(depth-2)
        break
    if node in visited: continue
    visited.add(node)
    orbiters = [b for (a,b) in orbit_definitions if a == node]
    orbited = [a for (a,b) in orbit_definitions if b == node]
    queue.extend([(orbiter, depth+1) for orbiter in orbiters])
    queue.extend([(orbiter, depth+1) for orbiter in orbited])

