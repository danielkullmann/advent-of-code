#!/usr/bin/env python3
"""
"""

fh = open("08.txt", "r")
content = fh.read().strip()
fh.close()
#content = "3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0"
#content = "3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0"
content = [int(x) for x in content]

width = 25
height = 6

pixels_per_layer = width*height

layers = [content[layer*pixels_per_layer:(layer+1)*pixels_per_layer] for layer in range(len(content)//pixels_per_layer)]

min_zeroes = None
result = None
for layer in layers:
    #print(len(layer), layer)
    zeroes = sum([1 for pixel in layer if pixel == 0])
    if min_zeroes is None or zeroes < min_zeroes:
        min_zeroes = zeroes
        ones = sum([1 for pixel in layer if pixel == 1])
        twos = sum([1 for pixel in layer if pixel == 2])
        result = ones*twos
print(min_zeroes, result)
