#!/usr/bin/env python3
"""
"""

def parse_op_code(input):
    opcode = input % 100
    input = input // 100
    imm1 = (input % 10) == 1
    input = input // 10
    imm2 = (input % 10) == 1
    input = input // 10
    imm3 = (input % 10) == 1
    return (opcode, imm1, imm2, imm3)

assert(parse_op_code(1002) == (2, False, True, False))

def get(content, value, immediate):
    if immediate:
        return value
    else:
        return content[value]

def run(content, pc, input):
    while True:
        (opcode, imm1, imm2, imm3) = parse_op_code(content[pc])
        if opcode == 1:
            # addition
            p1 = content[pc+1]
            p2 = content[pc+2]
            p3 = content[pc+3]
            content[p3] = get(content, p1, imm1) + get(content, p2, imm2)
            pc += 4
        elif opcode == 2:
            # multiplication
            p1 = content[pc+1]
            p2 = content[pc+2]
            p3 = content[pc+3]
            content[p3] = get(content, p1, imm1) * get(content, p2, imm2)
            pc += 4
        elif opcode == 3:
            p1 = content[pc+1]
            content[p1] = input.pop(0)
            pc += 2
        elif opcode == 4:
            p1 = content[pc+1]
            output = get(content, p1, imm1)
            pc += 2
            return (output, pc)
        elif opcode == 5:
            p1 = content[pc+1]
            p2 = content[pc+2]
            if get(content, p1, imm1) != 0:
                pc = get(content, p2, imm2)
            else:
                pc += 3
        elif opcode == 6:
            p1 = content[pc+1]
            p2 = content[pc+2]
            if get(content, p1, imm1) == 0:
                pc = get(content, p2, imm2)
            else:
                pc += 3
        elif opcode == 7:
            p1 = content[pc+1]
            p2 = content[pc+2]
            p3 = content[pc+3]
            if get(content, p1, imm1) < get(content, p2, imm2):
                content[p3] = 1
            else:
                content[p3] = 0
            pc += 4
        elif opcode == 8:
            p1 = content[pc+1]
            p2 = content[pc+2]
            p3 = content[pc+3]
            if get(content, p1, imm1) == get(content, p2, imm2):
                content[p3] = 1
            else:
                content[p3] = 0
            pc += 4
        elif opcode == 99:
            # program exit
            break
        else:
            raise Exception("Unknown opcode " + str(opcode))
    return (None, None)

fh = open("07.txt", "r")
content = fh.read()
fh.close()
#content = "3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0"
#content = "3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0"
content = [int(x) for x in content.split(",")]


max_value = 0
for phase0 in [5,6,7,8,9]:
    for phase1 in [5,6,7,8,9]:
        if phase1 == phase0: continue
        for phase2 in [5,6,7,8,9]:
            if phase2 == phase1: continue
            if phase2 == phase0: continue
            for phase3 in [5,6,7,8,9]:
                if phase3 == phase2: continue
                if phase3 == phase1: continue
                if phase3 == phase0: continue
                for phase4 in [5,6,7,8,9]:
                    if phase4 == phase3: continue
                    if phase4 == phase2: continue
                    if phase4 == phase1: continue
                    if phase4 == phase0: continue
                    content0 = content[:]; content1 = content[:]; content2 = content[:]; content3 = content[:]; content4 = content[:];
                    value4 = 0
                    pc0 = 0; pc1 = 0; pc2 = 0; pc3 = 0; pc4 = 0;
                    input0 = [phase0]; input1 = [phase1]; input2 = [phase2]; input3 = [phase3]; input4 = [phase4]
                    while True:
                        input0.append(value4)
                        (value0, pc0) = run(content0, pc0, input0)
                        input1.append(value0)
                        (value1, pc1) = run(content1, pc1, input1)
                        input2.append(value1)
                        (value2, pc2) = run(content2, pc2, input2)
                        input3.append(value2)
                        (value3, pc3) = run(content3, pc3, input3)
                        input4.append(value3)
                        (value4, pc4) = run(content4, pc4, input4)
                        if value4 == None:
                            break
                        max_value = max(max_value, value4)

print(max_value)

