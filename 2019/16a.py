#!/usr/bin/env python3
from itertools import islice

def pattern(step):
    """step is 0, 1, 2, 3 ..."""
    first = True
    base_pattern = [0, 1, 0, -1]
    while True:
        for value in base_pattern:
            for i in range(step+1):
                if not first:
                    yield value
                first = False

#assert((-17 % 10) == -7)
assert([1, 0, -1, 0, 1, 0, -1, 0, 1, 0] == list(islice(pattern(0),10)))
assert([0, 1, 1, 0, 0, -1, -1, 0, 0, 1] == list(islice(pattern(1),10)))
assert([0, 0, 1, 1, 1, 0, 0, 0, -1, -1] == list(islice(pattern(2),10)))

fh = open("16.txt")
numbers = list(map(int, [c for c in fh.read().strip()]))
fh.close()
#numbers = list(map(int, [c for c in "12345678"]))

def mul(tup):
    (a,b) = tup
    return a*b

#print(numbers)
for phase in range(100):
    new_numbers = []
    for i in range(len(numbers)):
        result = sum(map(mul, zip(numbers, pattern(i))))
        new_numbers.append(abs(result) % 10)
    numbers = new_numbers
    #print(numbers)
#print(numbers)
print("".join(map(str, numbers[0:8])))