"""
Each room consists of an encrypted name (lowercase letters separated by dashes) followed by a dash, a sector ID, and a checksum in square brackets.

A room is real (not a decoy) if the checksum is the five most common letters in the encrypted name, in order, with ties broken by alphabetization. For example:

    aaaaa-bbb-z-y-x-123[abxyz] is a real room because the most common letters are a (5), b (3), and then a tie between x, y, and z, which are listed alphabetically.
    a-b-c-d-e-f-g-h-987[abcde] is a real room because although the letters are all tied (1 of each), the first five are listed alphabetically.
    not-a-real-room-404[oarel] is a real room.
    totally-real-room-200[decoy] is not.

Of the real rooms from the list above, the sum of their sector IDs is 1514.

What is the sum of the sector IDs of the real rooms?
"""

from collections import Counter
import re

RE = re.compile("^([a-z\\-]*)-([0-9]*)\\[([a-z]*)\\]$")

def parse(line):
    match = RE.match(line.strip())
    return list(match.groups())

fh = open("04.txt", "r")
input = map(parse, fh.read().strip().split("\n"))
fh.close()

# input = list(map(parse, [
#     "aaaaa-bbb-z-y-x-123[abxyz]",
#     "a-b-c-d-e-f-g-h-987[abcde]",
#     "not-a-real-room-404[oarel]",
#     "totally-real-room-200[decoy]"
# ]))

def check(room):
    (name, _, checksum) = room
    counter = Counter(name)
    del counter["-"]
    counter = counter.most_common()
    counter.sort(key = lambda x: (-x[1], x[0]))
    real_checksum = "".join(map(lambda x: x[0], counter[0:5]))
    print(name, checksum, real_checksum)
    return checksum == real_checksum

sum = 0
for room in input:
    if check(room):
        sum += int(room[1])

print(sum)