"""
"""

import re

fh = open("03.txt", "r")
definitions = [list(map(int, re.split(" +", line.strip()))) for line in fh.read().strip().split("\n")]
fh.close()

def check(definition):
    (a, b, c) = definition
    if a+b <= c: return False
    if a+c <= b: return False
    if b+c <= a: return False
    return True

count = 0
for definition in definitions:
    if check(definition): 
        count += 1

print(count)