"""
"""

fh = open("02.txt", "r")
instructions = fh.read().strip().split("\n")
fh.close()

#instructions = ["ULL", "RRDDD", "LURDL", "UUUUD"]

pad = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
]

def find_position(number):
    for (row_no, row) in enumerate(pad):
        col_no = -1
        try:
            col_no = row.index(number)
        except Exception:
            pass
        if col_no >= 0:
            return (col_no, row_no)
    raise Exception("number not found: " + str(number))

def move(current_number, move):
    (col_no, row_no) = find_position(current_number)
    if move == "L":
        if col_no > 0: col_no -= 1
    elif move == "R":
        if col_no < 2: col_no += 1
    elif move == "U":
        if row_no > 0: row_no -= 1
    elif move == "D":
        if row_no < 2: row_no += 1
    else:
        raise Exception("Unknown move " + move)
    return pad[row_no][col_no]

number = 5
for instruction in instructions:
    for single_move in instruction:
        number = move(number, single_move)
    print(number,end="")
print("")
