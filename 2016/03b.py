"""
"""

import re

fh = open("03.txt", "r")
definitions = [list(map(int, re.split(" +", line.strip()))) for line in fh.read().strip().split("\n")]
fh.close()

# definitions = [
#     [101, 301, 501],
#     [102, 302, 502],
#     [103, 303, 503],
#     [201, 401, 601],
#     [202, 402, 602],
#     [203, 403, 603]
# ]

def transpose(definitions):
    result = []
    for index in range(0, len(definitions), 3):
        row1 = definitions[index]
        row2 = definitions[index+1]
        row3 = definitions[index+2]
        result.extend(zip(row1, row2, row3))
    return result

def check(definition):
    (a, b, c) = definition
    if a+b <= c: return False
    if a+c <= b: return False
    if b+c <= a: return False
    return True

count = 0
for definition in transpose(definitions):
    if check(definition): 
        count += 1

print(count)