#!/usr/bin/env python3

from itertools import permutations

"""Alice would gain 54 happiness units by sitting next to Bob."""
def parse(line):
    parts = line.strip(".").split(" ")
    sign = 1 if parts[2] == "gain" else -1
    return (parts[0], parts[10], sign * int(parts[3]))

fh = open("13.txt")
content = [parse(line.strip()) for line in fh.readlines()]
fh.close()

def calc_happiness(arrangement, happiness_data):
    result = 0
    for index in range(len(arrangement)):
        p1 = arrangement[index]
        p2 = arrangement[(index+1)%len(arrangement)]
        p3 = arrangement[(index-1)%len(arrangement)]
        result += happiness_data.get((p1,p2), 0)
        result += happiness_data.get((p1,p3), 0)
    return result


persons = set()
persons.update([a for (a,b,c) in content])
persons.update([b for (a,b,c) in content])
happiness_data = {(a,b): c for (a,b,c) in content}

max_happiness = None
for permutation in permutations(persons):
    happiness = calc_happiness(permutation, happiness_data)
    if max_happiness is None or happiness > max_happiness:
        max_happiness = happiness
        print(permutation)
print(max_happiness)
