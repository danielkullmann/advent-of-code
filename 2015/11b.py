password = "hepxcrrq"

letters = "abcdefghijklmnopqrstuvwxyz"
allowed_letters = "abcdefghjkmnpqrstuvwxyz"


def check1(password):
    """Passwords must include one increasing straight of at least three
    letters, like abc, bcd, cde, and so on, up to xyz. They cannot skip
    letters; abd doesn't count."""
    indices = [letters.index(c) for c in password]
    last_num = None
    current_streak = 0
    for value in indices:
        if last_num is None:
            current_streak = 1
        elif value == last_num+1:
            current_streak += 1
            if current_streak == 3:
                return True
        else:
            current_streak = 1
        last_num = value

    return False

def check2(password):
    """"Passwords may not contain the letters i, o, or l, as these letters can
    be mistaken for other characters and are therefore confusing."""
    pass

def check3(password):
    """ Passwords must contain at least two different, non-overlapping pairs of
    letters, like aa, bb, or zz. """
    first_match_index = None
    for index in range(0, len(password)-1):
        char = password[index]
        if char == password[index+1]:
            if first_match_index is None:
                first_match_index = index+1
            else:
                if char == password[first_match_index]:
                    if index > first_match_index+1:
                        return True
                else:
                    return True
    return False

def increase(password):
    increase = True
    result = ""
    for c in password[::-1]:
        if increase:
            index = allowed_letters.index(c)
            if index+1 < len(allowed_letters):
                result = allowed_letters[index+1] + result
                increase = False
            else:
                result = allowed_letters[0] + result
        else:
            result = c + result
    return result

assert(False == check1(password))
assert(True == check1("passturd"))
assert(False == check3(password))
assert(False == check3("passsord"))
assert(True == check3("passwood"))

while True:
    password = increase(password)
    if check1(password) and check3(password):
        break
while True:
    password = increase(password)
    if check1(password) and check3(password):
        break
print(password)
