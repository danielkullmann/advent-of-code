"""
"""

fh = open("05.txt")
content = fh.read().strip().split("\n")
fh.close()

def is_nice(line):
    print(line)
    found_double_pair = False
    for i in range(0, len(line)-3):
        sub = line[i:i+2]
        try:
            line.index(sub,i+2)
            print("  fd", sub)
            found_double_pair = True
            break
        except Exception:
            pass
    if not found_double_pair:
        print("  double pair")
        return False

    double_letter = False
    for i in range(0, len(line)-2):
        if line[i] == line[i+2]:
            double_letter = True
            break
    if not double_letter:
        print("  double")
        return False
    return True

count = 0
for line in content:
    if is_nice(line):
        count += 1
print(count)