"""
"""

fh = open("03.txt")
content = fh.read().strip()
fh.close()

def move(position, c):
    if c == "^":
        position = (position[0], position[1]-1)
    if c == "v":
        position = (position[0], position[1]+1)
    if c == "<":
        position = (position[0]-1, position[1])
    if c == ">":
        position = (position[0]+1, position[1])
    return position

positions = [(0,0), (0,0)]
index = 0
visited = set()
visited.add(positions[index])
for c in content:
    positions[index] = move(positions[index], c)
    visited.add(positions[index])
    index = 1-index

print(len(visited))