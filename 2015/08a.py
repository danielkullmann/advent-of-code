#!/usr/bin/env python3

fh = open("08.txt")
lines = map(lambda s: s.strip(), fh.readlines())
fh.close()

def calc(line):
    line = line[1:-1]
    count = 0
    index = 0
    while index<len(line):
        if line[index] == "\\":
            if line[index+1] == "x":
                index += 4
                count += 1
            else:
                index += 2
                count += 1
        else:
            count += 1
            index += 1
    return count

diff = 0
for line in lines:
    code_size = len(line)
    string_size = calc(line)
    print(code_size, string_size, line)
    diff += code_size-string_size
print(diff)

