"""
"""

from hashlib import md5

input = b"bgvyzdsv"
number = 0
while True:
    m = md5()
    m.update(input)
    m.update(bytes(str(number), "utf-8"))
    if m.hexdigest().startswith("00000"):
        print(number)
        break
    number += 1
