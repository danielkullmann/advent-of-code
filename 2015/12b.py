
import json
fh = open("12.txt")
data = json.load(fh)
fh.close()

def calculate_sum(data):
    result = 0
    tpe = type(data)
    if tpe == int:
        result = data
    elif tpe == list:
        result = sum([calculate_sum(item) for item in data])
    elif tpe == dict:
        if "red" not in data.values():
            result = sum([calculate_sum(item) for item in data.values()])
    return result

print(calculate_sum(data))

