"""
"""

fh = open("02.txt")
content = [map(int, line.split("x")) for line in fh.read().strip().split("\n")]
fh.close()

ribbon = 0
for (a,b,c) in content:
    options = [a+b, a+c, b+c]
    ribbon += 2*min(options) + a*b*c
print(ribbon)
