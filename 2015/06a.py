"""
"""

def parse(line):
    """turn on 489,959 through 759,964"""
    words = line.split(" ")
    if len(words) == 5:
        what = words[1]
        start = list(map(int,words[2].split(",")))
        end = list(map(int,words[4].split(",")))
    else:
        what = words[0]
        start = list(map(int,words[1].split(",")))
        end = list(map(int,words[3].split(",")))
    return (what, start, end)

fh = open("06.txt")
content = [parse(line) for line in fh.read().strip().split("\n")]
fh.close()

lights = [[0 for _ in range(1000)] for _ in range(1000)]
print("created")

for command in content:
    (what, (x1, y1), (x2,y2)) = command
    for x in range(x1, x2+1):
        for y in range(y1,y2+1):
            if what == "on":
                lights[y][x] = 1
            if what == "off":
                lights[y][x] = 0
            if what == "toggle":
                lights[y][x] = 1-lights[y][x]
print(sum([sum(row) for row in lights]))
