"""
"""

fh = open("05.txt")
content = fh.read().strip().split("\n")
fh.close()

def is_nice(line):
    print(line)
    vowels = len(list(filter(lambda c: c in "aeiou", line)))
    if vowels < 3:
        print("  vowels")
        return False
    double_letter = False
    for i in range(0, len(line)-1):
        if line[i] == line[i+1]:
            double_letter = True
            break
    if not double_letter:
        print("  double")
        return False
    for dont in ["ab", "cd", "pq", "xy"]:
        try:
            line.index(dont)
            print("  dont " + dont)
            return False
        except Exception:
            pass
    return True

count = 0
for line in content:
    if is_nice(line):
        count += 1
print(count)