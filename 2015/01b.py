"""
"""

fh = open("01.txt")
content = fh.read().strip()
fh.close()

position = 1
floor = 0
for c in content:
    if c == "(":
        floor += 1
    if c == ")":
        floor -= 1
        if floor == -1:
            print(position)
            break
    position += 1