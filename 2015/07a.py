"""
"""

def maybeParseInt(str):
    try:
        return int(str)
    except Exception:
        return str

def parse(line):
    (calc, to) = line.split(" -> ")
    calc = calc.split(" ")
    if len(calc) == 1:
        return (to, "SET", maybeParseInt(calc[0]), None)
    elif len(calc) == 2:
        if calc[0] not in "NOT".split(","): raise Exception("Unsupported command", calc[1])
        return (to, calc[0], maybeParseInt(calc[1]), None)
    elif len(calc) == 3:
        if calc[1] not in "OR,AND,RSHIFT,LSHIFT".split(","): raise Exception("Unsupported command", calc[1])
        return (to, calc[1], maybeParseInt(calc[0]), maybeParseInt(calc[2]))
    else:
        raise Exception("Unknown calc", calc)

def is_int(value):
    return isinstance(value, int)

fh = open("07.txt")
input = [parse(line) for line in fh.read().strip().split("\n")]
fh.close()

wires = dict()

def has_value(line):
    (_, command, from1, from2) = line
    check1 = is_int(from1) or from1 in wires
    check2 = from2 is None or is_int(from2) or from2 in wires
    return check1 and check2

def get_value(wire):
    if is_int(wire):
        return wire
    if wire in wires:
        return wires[wire]
    raise Exception("wire has no value", wire)

while True:
    wire_lines_with_values = [line for line in input if has_value(line)]
    if "a" in wires:
        print(wires["a"])
        break
    for line in wire_lines_with_values:
        (to, command, from1, from2) = line
        if command == "SET":
            if from2 is not None:
                raise Exception("SET is wrong")
            wires[to] = get_value(from1)
        elif command == "NOT":
            if from2 is not None:
                raise Exception("NOT is wrong")
            wires[to] = ~ get_value(from1)
        elif command == "OR":
            wires[to] = get_value(from1) | get_value(from2)
        elif command == "AND":
            wires[to] = get_value(from1) & get_value(from2)
        elif command == "LSHIFT":
            wires[to] = get_value(from1) << get_value(from2)
        elif command == "RSHIFT":
            wires[to] = get_value(from1) >> get_value(from2)
        else:
            raise Exception("Unknown command", command)
        input.remove(line)

#map = {command[0]: command for command in content}
#print(map)
# # Going backwards is not possible, maybe due to circles in the wire..
# def get_value(wire):
#     if wire == None:
#         raise Exception("wire is None")
#     if isinstance(wire, int): return wire
#     if not isinstance(wire, str): raise Exception("Unsupported wire", wire)
#     # Command (to, command, from1, from2)
#     (_, command, from1, from2) = map[wire]
#     #print("command", _, command, from1, from2)
#     if command == "SET":
#         if from2 is not None:
#             raise Exception("SET is wrong")
#         return get_value(from1)
#     elif command == "NOT":
#         if from2 is not None:
#             raise Exception("NOT is wrong")
#         return ~ get_value(from1)
#     elif command == "OR":
#         return get_value(from1) | get_value(from2)
#     elif command == "AND":
#         return get_value(from1) & get_value(from2)
#     elif command == "LSHIFT":
#         return get_value(from1) << get_value(from2)
#     elif command == "RSHIFT":
#         return get_value(from1) >> get_value(from2)
#     else:
#         raise Exception("Unknown command", command)

# print(get_value("a"))
