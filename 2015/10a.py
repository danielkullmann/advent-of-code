#!/usr/bin/env python3

number = "1113222113"
#number = "1"

def rle(str):
    result = []
    current_num = None
    current_count = 0
    for c in str:
        if current_num is None:
            current_num = c
            current_count = 1
        elif c == current_num:
            current_count += 1
        else:
            result.append((current_count, current_num))
            current_num = c
            current_count = 1
    if current_num is not None:
       result.append((current_count, current_num))
    return result

def combine(rle):
    return "".join([str(a) + b for (a,b) in rle])

#print(number)
#print(rle(number))
#print(combine(rle(number)))
#print(rle(combine(rle(number))))
#print(combine(rle(combine(rle(number)))))

for i in range(40):
    number = combine(rle(number))
    print(len(number))

