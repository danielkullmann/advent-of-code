"""
"""

fh = open("02.txt")
content = [map(int, line.split("x")) for line in fh.read().strip().split("\n")]
fh.close()

area = 0
for (a,b,c) in content:
    sides = [a*b, a*c, b*c]
    area += 2*sum(sides) + min(sides)
print(area)
