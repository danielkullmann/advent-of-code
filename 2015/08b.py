#!/usr/bin/env python3

fh = open("08.txt")
lines = map(lambda s: s.strip(), fh.readlines())
fh.close()

def calc(line):
    result = ""
    for c in line:
        if c in ['"', '\\']:
            result += '\\' + c
        else:
            result += c
    return '"' + result + '"'

diff = 0
for line in lines:
    encoded = calc(line)
    print(line, encoded)
    diff += len(encoded) - len(line)
print(diff)

