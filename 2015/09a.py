#!/usr/bin/env python3

fh = open("09.txt")
lines = map(lambda s: s.strip(), fh.readlines())
#lines = [
#    "London to Dublin = 464",
#    "London to Belfast = 518",
#    "Dublin to Belfast = 141"
#]
fh.close()

cities = set()
distances = []
for line in lines:
    parts = line.split(" ")
    city1 = parts[0]
    city2 = parts[2]
    distance = int(parts[4])
    cities.add(city1)
    cities.add(city2)
    distances.append((city1, city2, distance))
    distances.append((city2, city1, distance))

def calc_distance(city1, city2, distances):
    min_distance = None
    min_route = None
    queue = [(city1, 0, [city1])]
    while len(queue) > 0:
        (city, distance, visited) = queue.pop()
        if city == city2 and len(visited) == len(cities):
            if min_distance is None or distance < min_distance:
                min_distance = distance
                min_route = visited[:]
        else:
            neighbors = [(b,c) for (a,b,c) in distances if a == city]
            for (neighbor, add_distance) in neighbors:
                if neighbor not in visited:
                    new_visited = visited[:]
                    new_visited.append(neighbor)
                    queue.append((neighbor, distance + add_distance, new_visited))
    return (min_distance, min_route)



solutions = []
for city1 in cities:
    for city2 in cities:
        if city1 >= city2:
            continue
        solution = calc_distance(city1, city2, distances)
        solutions.append(solution)

print(min(solutions))
