"""
"""

fh = open("03.txt")
content = fh.read().strip()
fh.close()

position = (0,0)
visited = set()
visited.add(position)
for c in content:
    if c == "^":
        position = (position[0], position[1]-1)
    if c == "v":
        position = (position[0], position[1]+1)
    if c == "<":
        position = (position[0]-1, position[1])
    if c == ">":
        position = (position[0]+1, position[1])
    visited.add(position)
print(len(visited))