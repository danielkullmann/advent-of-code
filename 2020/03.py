with open("03.txt") as fh:
    input = [line.strip() for line in fh.readlines()]

test = """..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#"""

if False:
    input = [s.strip() for s in test.split("\n")]

#print("\n".join(input))

num_trees = 0

n = len(input[0])
col = 0
for row in range(len(input)):
    while col >= n: 
        col -= n
    if input[row][col] == "#":
        num_trees += 1
    col += 3

print(num_trees)

slopes = [
    (1,1),
    (3,1),
    (5,1),
    (7,1),
    (1,2)
]

result = 1
for dy, dx in slopes:
    row = 0
    col = 0
    num_trees = 0
    while row < len(input):
        while col >= n: 
            col -= n
        if input[row][col] == "#":
            num_trees += 1
        col += dy
        row += dx
    print(dy,dx,num_trees)
    result *= num_trees

print(result)
        