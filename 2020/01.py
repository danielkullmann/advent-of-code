with open("01.txt") as fh:
    input = [int(line) for line in fh.readlines()]

all = set(input)

for i in range(len(input)):
    for j in range(i+1, len(input)):
        if input[i] + input[j] == 2020:
            print(input[i], input[j], input[i] * input[j])
            break

for i in range(len(input)):
    for j in range(i+1, len(input)):
        missing = 2020 - input[i] - input[j]
        if missing in all:
            print(input[i], input[j], missing, input[i] + input[j] + missing, input[i] * input[j] * missing)
