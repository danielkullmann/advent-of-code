import re

RE_LINE = re.compile("^([0-9]+)-([0-9]+) (.): (.+)$")

def parse(line):
    match = RE_LINE.match(line)
    if not match:
        raise Exception("can't parse", line)
    return (int(match.group(1)), int(match.group(2)), match.group(3), match.group(4))

with open("02.txt") as fh:
    input = [parse(line) for line in fh.readlines()]

num_valid = 0

for min, max, char, password in input:
    num = len([c for c in password if c == char])
    #print(min, num, max, char, password)
    if min <= num <= max:
        num_valid += 1

print(len(input), num_valid)

num_valid = 0

for pos1, pos2, char, password in input:
    valid1 = pos1-1 < len(password) and password[pos1-1] == char
    valid2 = pos2-1 < len(password) and password[pos2-1] == char
    
    if (valid1 or valid2) and not (valid1 and valid2):
        num_valid += 1

print(len(input), num_valid)


"""
1-3 a: abcde
1-3 b: cdefg
2-9 c: ccccccccc
"""

