testInput = """FBFBBFFRLR
BFFFBBFRRR
FFFBBBFRRR
BBFFBBFRLL"""

testInput = [line.strip() for line in testInput.split("\n")]

with open("05.txt") as fh:
    input = [line.strip() for line in fh.readlines()]

def q1(input):
    max_num = 0
    for line in input:
        rows = line[0:7]
        seats = line[7:]
        rows = rows.replace("F", "0").replace("B", "1")
        seats = seats.replace("L", "0").replace("R", "1")
        rows = int(rows, 2)
        seats = int(seats, 2)
        num = rows*8 + seats
        max_num = max(max_num, num)

    print(max_num)

def q2(input):
    max_num = 0
    nums = set()
    for line in input:
        rows = line[0:7]
        seats = line[7:]
        rows = rows.replace("F", "0").replace("B", "1")
        seats = seats.replace("L", "0").replace("R", "1")
        rows = int(rows, 2)
        seats = int(seats, 2)
        num = rows*8 + seats
        nums.add(num)
        max_num = max(max_num, num)

    for n in range(1,max_num):
        if n in nums:
            continue
        if n+1 in nums and n-1 in nums:
            print(n)
            break

q1(testInput)
q1(input)

q2(input)
