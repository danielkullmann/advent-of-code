testInput = """ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in"""

testInput = [line.strip() for line in testInput.split("\n")]

testInput2 = """eyr:1972 cid:100
hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

iyr:2019
hcl:#602927 eyr:1967 hgt:170cm
ecl:grn pid:012533040 byr:1946

hcl:dab227 iyr:2012
ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

hgt:59cm ecl:zzz
eyr:2038 hcl:74454a iyr:2023
pid:3556412378 byr:2007"""

testInput2 = [line.strip() for line in testInput2.split("\n")]


testInput3 = """pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
hcl:#623a2f

eyr:2029 ecl:blu cid:129 byr:1989
iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

hcl:#888785
hgt:164cm byr:2001 iyr:2015 cid:88
pid:545766238 ecl:hzl
eyr:2022

iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719"""

testInput3 = [line.strip() for line in testInput3.split("\n")]

with open("04.txt") as fh:
    input = [line.strip() for line in fh.readlines()]

def parse(lines):
    passports = []
    passport = {}
    for line in lines:
        if line == "":
            if len(passport) > 0:
                passports.append(passport)
            passport = {}
            continue
        parts = line.split(" ")
        for part in parts:
            if part == "":
                continue
            k, v = part.split(":", 1)
            passport[k] = v
    if len(passport) > 0:
        passports.append(passport)

    return passports

mandatory_fields = set("byr iyr eyr hgt hcl ecl pid".split(" "))

def check_valid(passport):
    num_mandatory_fields = 0
    for k,v in passport.items():
        if k in mandatory_fields:
            num_mandatory_fields += 1
    return num_mandatory_fields == len(mandatory_fields)


def check_valid_2(passport):
    num_mandatory_fields = 0
    for k,v in passport.items():
        if k in mandatory_fields:
            num_mandatory_fields += 1
        if k == "byr":
            if len(v) != 4:
                return False
            v = int(v)
            if not 1920 <= v <= 2002:
                return False
        if k == "iyr":
            if len(v) != 4:
                return False
            v = int(v)
            if not 2010 <= v <= 2020:
                return False
        if k == "eyr":
            if len(v) != 4:
                return False
            v = int(v)
            if not 2020 <= v <= 2030:
                return False
        if k == "hgt":
            if v.endswith("in"):
                v = int(v[:-2])
                if not 59 <= v <= 76:
                    return False
            elif v.endswith("cm"):
                v = int(v[:-2])
                if not 150 <= v <= 193:
                    return False
            else:
                return False
        if k == "hcl":
            if len(v) != 7:
                return False
            if v[0] != "#":
                return False
            for c in v[1:]:
                if c not in "0123456789abcdef":
                    return False
        if k == "ecl":
            if v not in set(["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]):
                return False
        if k == "pid":
            if len(v) != 9:
                return False
            v = int(v)
            if not v >= 0:
                return False

    return num_mandatory_fields == len(mandatory_fields)

def q1(input):
    passports = parse(input)
    valid = 0
    for p in passports:
        if check_valid(p):
            valid += 1
    print(len(passports), valid)

def q2(input):
    passports = parse(input)
    valid = 0
    for p in passports:
        if check_valid_2(p):
            valid += 1
    print(len(passports), valid)

q1(testInput)
q1(input)

q2(testInput)
q2(testInput2)
q2(testInput3)
q2(input)
