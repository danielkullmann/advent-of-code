#!/usr

"""
"""

fh = open("08.txt", "r")
content = fh.read()
fh.close()

input = [int(x) for x in content.strip().split(" ")]

sum_metadata = 0

def read_node(input, index):
    global sum_metadata
    num_children = input[index]
    num_metadata = input[index+1]
    index += 2
    for i in range(num_children):
        index = read_node(input, index)
    for i in range(num_metadata):
        sum_metadata += input[index]
        index  += 1
    return index

index = read_node(input, 0)
print(index, len(input), sum_metadata)
