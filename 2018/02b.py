#!/usr/bin/env python3

"""
Checksums
"""

from collections import Counter

fh = open("02.txt", "r")
content = fh.read().strip().split("\n")
fh.close()

def differ(a, b):
    if a != b:
        return 1
    return 0
twos = 0
threes = 0
for s in content:
    for t in content:
        zipped = zip(s, t)
        diffs = sum([differ(a,b) for (a,b) in zipped])
        if diffs == 1:
            print(s, t)

