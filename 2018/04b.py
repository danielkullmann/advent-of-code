#!/usr/bin/env python3

"""
"""
from collections import Counter, defaultdict
import re

RE_LINE = re.compile("^\\[([0-9]*)-([0-9]*)-([0-9]*) ([0-9]*):([0-9]*)\\] (.*)$")
RE_GUARD = re.compile("Guard #([0-9]*) begins shift")
TXT_FALLS_ASLEEP = "falls asleep"
TXT_WAKES_UP = "wakes up"

def parse(s):
    match = RE_LINE.match(s)
    (year, month, day, hour, minute, text) = match.groups()
    return (int(year), int(month), int(day), int(hour), int(minute), text)

fh = open("04.txt", "r")
input = [x for x in fh.read().strip().split("\n")]
input.sort()
input = map(parse, input)
fh.close()

class Guard:
    def __init__(self):
        self.id = None
        self.num_minutes = 0
        self.minute_counter = Counter()

    def __repr__(self):
        return str(self.id) + " " + str(self.num_minutes) + " " + str(self.minute_counter)

guards = defaultdict(Guard)
guard = None
asleep = None
for line in input:
    (year, month, day, hour, minute, text) = line
    match = RE_GUARD.match(text)
    if match is not None:
        id = int(match.group(1))
        guard = guards[id]
        guard.id = id
    elif text == TXT_FALLS_ASLEEP:
        asleep = minute
    elif text == TXT_WAKES_UP:
        guard.num_minutes += minute-asleep
        guard.minute_counter.update(range(asleep, minute))
    else:
        raise Exception("Unknown text " + text)

def safe_list_get (l, idx, default):
  try:
    return l[idx]
  except IndexError:
    return default

guards = list(guards.values())
guards.sort(key=lambda g: safe_list_get(g.minute_counter.most_common(1), 0, [0, 0])[1])

id = guards[-1].id
minute = guards[-1].minute_counter.most_common(1)[0][0]
print(id, minute, id*minute)

