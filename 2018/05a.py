#!/usr/bin/env python3

"""
"""

fh = open("05.txt", "r")
content = [c for c in fh.read().strip()]
fh.close()

index = 0
orig = len(content)
while index+1 < len(content):
    a = content[index]
    b = content[index+1]
    if a != b and a.lower() == b.lower():
        content.pop(index)
        content.pop(index)
        index -= 1
    else:
        index += 1
print(len(content))


