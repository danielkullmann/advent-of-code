#!/usr/bin/env python3

"""
"""

import re

RE = re.compile("#([0-9]*) @ ([0-9]*),([0-9]*): ([0-9]*)x([0-9]*)")

def parse_claim(s):
    match = RE.match(s)
    return list(map(int, match.groups()))

fh = open("03.txt", "r")
claims = [parse_claim(x) for x in fh.read().strip().split("\n")]
fh.close()


#claims = """#1 @ 1,3: 4x4
##2 @ 3,1: 4x4
##3 @ 5,5: 2x2
#"""
#claims = [parse_claim(x) for x in claims.strip().split("\n")]

# Overlaps:
# 1.
# <----->......>
#     <----->
# 2. 
#     <------>
# <----->.......>

def get_x_overlap(claim1, claim2):
    (id1, x1, y1, w1, h1) = claim1
    (id2, x2, y2, w2, h2) = claim2
    if x1 <= x2:
        if x1+w1 <= x2: return None
        return (x2, min(x1+w1, x2+w2))
    else:
        return get_x_overlap(claim2, claim1)

def get_y_overlap(claim1, claim2):
    (id1, x1, y1, w1, h1) = claim1
    (id2, x2, y2, w2, h2) = claim2
    if y1 <= y2:
        if y1+h1 <= y2: return None
        return (y2, min(y1+h1, y2+h2))
    else:
        return get_y_overlap(claim2, claim1)

def get_overlap(claim1, claim2):
    (id1, x1, y1, w1, h1) = claim1
    (id2, x2, y2, w2, h2) = claim2
    if id1 == id2: return None
    xo = get_x_overlap(claim1,claim2)
    if xo == None: return None
    yo = get_y_overlap(claim1,claim2)
    if yo == None: return None
    return (xo[0], yo[0], xo[1], yo[1])

for c1 in claims:
    has_overlap = False
    for c2 in claims:
        overlap = get_overlap(c1, c2)
        if overlap is not None:
            has_overlap = True
            break
    if not has_overlap:
        print(c1)

