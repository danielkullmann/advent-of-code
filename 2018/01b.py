#!/usr/bin/env python3

"""
Sum values, find duplicated value
"""

fh = open("01.txt", "r")
content = fh.read().strip().split("\n")
fh.close()

#content = "+1,-2,+3,+1".split(",")

def calc(content):
    freq = 0
    freqs = set([freq])
    while True:
        for x in content:
            freq += int(x)
            if freq in freqs:
                print(freq)
                return
            freqs.add(freq)

calc(content)
