"""479 players; last marble is worth 71035 points"""

from collections import defaultdict

def play(num_players, last_marble):
    scores = defaultdict(int)
    board = [0]
    player = 1
    current = 0
    for marble in range(1, last_marble+1):
        if marble % 23 == 0:
            scores[player] += marble
            current = (current - 7) % len(board)
            extra_marble = board.pop(current)
            scores[player] += extra_marble
        else:
            current = (current + 2) % len(board)
            if current == 0:
                board.append(marble)
                current = len(board)-1
            else:
                board.insert(current, marble)
            pass
        # print(player, current, board)
        player = (player % num_players) +1
    scores = list(scores.items())
    scores.sort(key=lambda item: -item[1])
    return scores[0][1]

print(play(479, 7103500))