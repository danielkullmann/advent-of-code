"""
"""

import re

# position=<-50429,  40580> velocity=< 5, -4>
RE = re.compile("position=< *([-+]?[0-9]+), *([-+]?[0-9]+)> velocity=< *([-+]?[0-9]+), *([-+]?[0-9]+)>")

def parse(line):
    match = RE.match(line)
    return list(map(int, match.groups()))

def move(points, definitions):
    for item in enumerate(zip(points, definitions)):
        (i, ((x,y), (_1, _2, dx, dy))) = item
        points[i] = (x+dx, y+dy)

def output(run, points, min_x, max_x, min_y, max_y):
    print(run, min_x, max_x, min_y, max_y)
    picture = [["." for _ in range(min_x, max_x+1)] for _ in range(min_y, max_y+1)]
    for (x,y) in points:
        picture[y-min_y][x-min_x] = "#"
    print("\n".join(["".join(line) for line in picture]))

fh = open("10.txt")
definitions = [parse(line) for line in fh.read().strip().split("\n")]

#for definition in definitions:
#    print(definition)

points = [(x,y) for (x,y,dx,dy) in definitions]

small = False
run = 1
while True:
    move(points, definitions)
    min_x = min(map(lambda i: i[0], points))
    max_x = max(map(lambda i: i[0], points))
    min_y = min(map(lambda i: i[1], points))
    max_y = max(map(lambda i: i[1], points))
    if abs(max_x-min_x) < 100 and abs(max_y-min_y) < 20:
        output(run, points, min_x, max_x, min_y, max_y)
        small = True
    elif small:
        break
    run += 1