turns = 556061 + 10 - 2
turns = 9 + 10 - 2

board = [3,7]
elfs = [0, 1]

for turn in range(turns):
    combined = board[elfs[0]] + board[elfs[1]]
    if combined >= 10:
        board.append(combined // 10)
    board.append(combined % 10)
    elfs[0] = (elfs[0] + board[elfs[0]] + 1) % len(board)
    elfs[1] = (elfs[1] + board[elfs[1]] + 1) % len(board)
    pass

print(board)
print(board[-11:-1])
