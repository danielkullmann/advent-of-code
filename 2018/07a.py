#!/usr/bin/env python3

import sys
from collections import defaultdict

input = """Step C must be finished before step A can begin.
Step C must be finished before step F can begin.
Step A must be finished before step B can begin.
Step A must be finished before step D can begin.
Step B must be finished before step E can begin.
Step D must be finished before step E can begin.
Step F must be finished before step E can begin."""

fh = open("07.txt", "r")
input = fh.read().strip()
fh.close()

lines = input.strip().split("\n")

steps = set()
dependencies = defaultdict(list)
for line in lines:
    words = line.split(" ")
    prereq = words[1]
    step = words[-3]
    steps.add(prereq)
    steps.add(step)
    dependencies[step].append(prereq)

while len(steps) > 0:
    can_be_done = sorted([step for step in steps if len(dependencies[step]) == 0])
    is_done = can_be_done[0]
    print(is_done,end="")
    steps.remove(is_done)
    for value in dependencies.values():
        if is_done in value:
            value.remove(is_done)
print("")
