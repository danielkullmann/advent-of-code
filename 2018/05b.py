#!/usr/bin/env python3

"""
"""

fh = open("05.txt", "r")
content_str = fh.read().strip()
content = [c for c in content_str]
fh.close()

chars = set(content_str.lower())

def remove_char(input, char):
    return [c for c in input if c.lower() != char]

def reduce(content):
    index = 0
    while index+1 < len(content):
        a = content[index]
        b = content[index+1]
        if a != b and a.lower() == b.lower():
            content.pop(index)
            content.pop(index)
            index -= 1
        else:
            index += 1
    return content

result = []
for char in chars:
    result.append(len(reduce(remove_char(content, char))))
print(min(result))


