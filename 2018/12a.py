"""
"""

fh = open("12.txt")

initial = fh.readline().split(":")[1].strip()
initial = "#..#.#..##......###...###"
# Check for empty line next
assert ("" == fh.readline().strip())

rules_content = fh.readlines()
# rules_content = """...## => #
# ..#.. => #
# .#... => #
# .#.#. => #
# .#.## => #
# .##.. => #
# .#### => #
# #.#.# => #
# #.### => #
# ##.#. => #
# ##.## => #
# ###.. => #
# ###.# => #
# ####. => #
# ..#.# => .
# #.#.. => .
# ..##. => .
# ..### => .
# ..### => .
# .###. => .
# """.strip().split("\n")
rules = [s.strip().split(" => ") for s in rules_content]

max_rule_length=max(map(lambda x: len(x[0]), rules))

# Index in initial that represents the index zero (the first pot)
zero_index = 0

while not initial.startswith("."*max_rule_length):
    initial = "." + initial
    zero_index += 1
while not initial.endswith("."*max_rule_length):
    initial = initial + "."


for rule in rules: print(rule)

generation = 1

state = initial
print (0, state)
while generation <= 20:
    # new_state = ""
    # for index in range(len(state)):
    #     value = state[index]
    #     for (pattern, pot_or_not) in rules:
    #         if state[index:].startswith(pattern):
    #             print(" >", index, pattern, pot_or_not)
    #             value = pot_or_not
    #             break
    #         pass
    #     new_state += value
    # state = new_state

    # Current state should not be changed, 
    # so we make a copy of it to be changed
    new_state = [c for c in state]
    for (pattern, pot_or_not) in rules:
        #print(" p", pattern, pot_or_not)
        index = 0
        while True:
            try:
                index = state.index(pattern, index)
                new_state[index+2] = pot_or_not
                #print(" >", index, "".join(new_state))
                index += 1
            except Exception as e:
                #print(e)
                # pattern can't be found => next pattern
                break
    state = "".join(new_state)
    while not state.startswith("."*max_rule_length):
        state = "." + state
        zero_index += 1
    while not state.endswith("."*max_rule_length):
        state = state + "."
    print(generation, state)
    generation += 1

print(state)
sum_pot_nums = 0
for index in range(len(state)):
    if state[index] == "#":
        sum_pot_nums += index - zero_index
print(sum_pot_nums)