"""
"""

serial = 3613
#serial = 18
#serial = 42

def calculate_power_level(x, y, serial):
    rack_id = x+10
    pl = rack_id * y
    pl += serial
    pl *= rack_id
    pl %= 1000
    pl //= 100
    pl -= 5
    return pl

assert(calculate_power_level(3,5,8) == 4)
assert(calculate_power_level(122,79,57) == -5)
assert(calculate_power_level(217,196,39) == 0)
assert(calculate_power_level(101,153,71) == 4)

cells = dict()
for x in range(1, 301):
    for y in range(1, 301):
        cells[(x,y)] = calculate_power_level(x, y, serial)

max_grid_level = None
max_grid = None
for x in range(1, 301-3):
    for y in range(1, 301-3):
        grid_level = 0
        for dx in range(0,3):
            for dy in range(0,3):
                grid_level += cells[(x+dx,y+dy)]
        if max_grid_level is None or grid_level > max_grid_level:
            max_grid_level = grid_level
            max_grid = (x,y)

print(max_grid_level)
print(max_grid)
