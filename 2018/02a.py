#!/usr/bin/env python3

"""
Checksums
"""

from collections import Counter

fh = open("02.txt", "r")
content = fh.read().strip().split("\n")
fh.close()

twos = 0
threes = 0
for s in content:
    counter = Counter(s)
    has_two = False
    has_three = False
    for (_,count) in counter.items():
        if count == 2:
            has_two = True
            if has_three: break
        if count == 3:
            has_three = True
            if has_two: break
    if has_two:
        twos += 1
    if has_three:
        threes += 1

print(twos*threes)
