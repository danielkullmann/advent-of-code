#!/usr/bin/env python3

"""
Sum values
"""

fh = open("01.txt", "r")
content = fh.read()
fh.close()

print(sum([int(x) for x in content.strip().split("\n")]))
