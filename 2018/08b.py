#!/usr

"""
"""

fh = open("08.txt", "r")
content = fh.read()
fh.close()

input = [int(x) for x in content.strip().split(" ")]

def read_node(input, index):
    num_children = input[index]
    num_metadata = input[index+1]
    index += 2
    child_values = dict()
    metadata = list()
    for i in range(num_children):
        (child_value, index) = read_node(input, index)
        child_values[i+1] = child_value
    for i in range(num_metadata):
        metadata.append(input[index])
        index  += 1
    if num_children == 0:
        return (sum(metadata), index)
    else:
        return (sum([child_values.get(i, 0) for i in metadata]), index)

(value, index) = read_node(input, 0)
print(value, index, len(input))
