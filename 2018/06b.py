#!/usr/bin/env python3

import sys
from collections import Counter

input = """1, 1
1, 6
8, 3
3, 4
5, 5
8, 9"""

fh = open("06.txt", "r")
input = fh.read().strip()
fh.close()

#coords = [(int(a), int(b)) for line in input.split("\n") for (a,b) in line.split(", ")]
coords = [tuple(map(int, line.split(", "))) for line in input.split("\n")]

x_coords = [e[0] for e in coords]
y_coords = [e[1] for e in coords]

x_min = min(x_coords)
x_max = max(x_coords)
y_min = min(y_coords)
y_max = max(y_coords)


def get_distance(p1, p2):
    return abs(p1[0]-p2[0]) + abs(p1[1]-p2[1])

def get_distance_sum(pos, coords):
    result = 0
    for coord in coords:
        distance = get_distance(pos, coord)
        result += distance
    return result


def calc(mult):
    start_x = x_min-mult*(x_max-x_min)
    end_x = x_max+mult*(x_max-x_min)
    start_y = y_min-mult*(y_max-y_min)
    end_y = y_max+mult*(y_max-y_min)

    result = 0
    for x in range(start_x, end_x+1):
        for y in range(start_y, end_y+1):
            ds = get_distance_sum((x,y), coords)
            if ds < 10000:
                result += 1
    return result

print(calc(2))
