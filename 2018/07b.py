#!/usr/bin/env python3

import sys
from collections import defaultdict

input = """Step C must be finished before step A can begin.
Step C must be finished before step F can begin.
Step A must be finished before step B can begin.
Step A must be finished before step D can begin.
Step B must be finished before step E can begin.
Step D must be finished before step E can begin.
Step F must be finished before step E can begin."""

fh = open("07.txt", "r")
input = fh.read().strip()
fh.close()

lines = input.strip().split("\n")

steps = set()
dependencies = defaultdict(list)
for line in lines:
    words = line.split(" ")
    prereq = words[1]
    step = words[-3]
    steps.add(prereq)
    steps.add(step)
    dependencies[step].append(prereq)

def get_next_step(timer, steps, dont_use):
    can_be_done = sorted([step for step in steps if len(dependencies[step]) == 0 and step not in dont_use])
    if len(can_be_done) == 0: return None
    print(timer, "next steps", can_be_done)
    return can_be_done[0]

def finish_step(is_done, steps):
    steps.remove(is_done)
    for value in dependencies.values():
        if is_done in value:
            value.remove(is_done)

def calc_time(step):
    return 61 + ord(step) - ord("A")

timer = 0
workers = [["", 0], ["", 0], ["", 0], ["", 0], ["", 0]]
while True:
    for worker_index in range(0, len(workers)):
       worker = workers[worker_index]
       if worker[0] != "":
           # One second of work passes
           worker[1] -= 1
           if worker[1] == 0:
               print(timer, "finished", worker[0], worker_index)
               finish_step(worker[0], steps)
               worker[0] = ""
    for worker_index in range(0, len(workers)):
       worker = workers[worker_index]
       if worker[0] == "":
           next_step = get_next_step(timer, steps, [a for (a,b) in workers if a != ""])
           if next_step is not None:
               worker[0] = next_step
               worker[1] = calc_time(next_step)
               print(timer, "start", worker, worker_index)
    if len(steps) == 0:
        break
    timer += 1

print(timer)
