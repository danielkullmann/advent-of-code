
testInput = """3,4,3,1,2"""

testInput = [int(s.strip()) for s in testInput.split(",")]

with open("06.txt") as fh:
    input = fh.read()
    input = [int(s.strip()) for s in input.split(",")]

def q1(input):
    fish = input[:]
    for _ in range(80):
        new_fish = []
        for i in range(len(fish)):
            if fish[i] == 0:
                fish[i] = 6
                new_fish.append(8)
            else:
                fish[i] -= 1
        fish = fish + new_fish
        #print(_, len(fish))
    print(len(fish))

def q2(input):
    fish = {}
    for i in input:
        if i in fish:
            fish[i] += 1
        else:
            fish[i] = 1
    for _ in range(256):
        new_fish = 0
        fish2 = {}
        for l, n in fish.items():
            if l == 0:
                if 6 in fish2:
                    fish2[6] += n
                else:
                    fish2[6] = n
                new_fish += n
            else:
                if l-1 in fish2:
                    fish2[l-1] += n
                else:
                    fish2[l-1] = n
        if 8 in fish2:
            fish2[8] += new_fish
        elif new_fish != 0:
            fish2[8] = new_fish
        fish = fish2
        #print(_, sum(fish.values()), sorted(fish.items()))
    print(sum(fish.values()))

q1(testInput)
q1(input)
q2(testInput)
q2(input)
