
testInput = """2199943210
3987894921
9856789892
8767896789
9899965678"""

testInput = [[int(e) for e in s.strip()] for s in testInput.split("\n")]

with open("09.txt") as fh:
    input = fh.read()
    input = [[int(e) for e in s.strip()] for s in input.split("\n") if s.strip() != ""]

def neighbors(grid, row, column):
    result = []
    if row-1 >= 0:
        result.append((row-1, column))
    if row+1 < len(grid):
        result.append((row+1, column))
    if column-1 >= 0:
        result.append((row, column-1))
    if column+1 < len(grid[row]):
        result.append((row, column+1))
    return result

def q1(grid):
    num_sum = 0
    for row in range(len(grid)):
        for column in range(len(grid[row])):
            num = grid[row][column]
            all_neighbors_are_higher = True
            for neighbor in neighbors(grid, row, column):
                r, c = neighbor
                if grid[r][c] <= num:
                    all_neighbors_are_higher = False
                    break
            if all_neighbors_are_higher:
                num_sum += num + 1
    print(num_sum)

def find_basin(grid, row, column, seen, basin):
    if (row, column) in seen:
        return
    seen.add((row, column))
    basin.add((row, column))
    num = grid[row][column]
    for r, c in neighbors(grid, row, column):
        if grid[r][c] < num or grid[r][c] == 9:
            continue
        find_basin(grid, r, c, seen, basin)

def q2(grid):
    basin_sizes = []
    seen = set()
    for row in range(len(grid)):
        for column in range(len(grid[row])):
            num = grid[row][column]
            if num == 9 or (row, column) in seen:
                continue

            all_neighbors_are_higher = True
            for neighbor in neighbors(grid, row, column):
                r, c = neighbor
                if grid[r][c] <= num:
                    all_neighbors_are_higher = False
                    break
            if all_neighbors_are_higher:
                basin = set()
                basin.add((row, column))
                find_basin(grid, row, column, seen, basin)
                basin_sizes.append(len(basin))

    basin_sizes.sort(reverse=True)
    result = 1
    for n in basin_sizes[0:3]:
        result *= n
    print(result)

q1(testInput)
q1(input)
q2(testInput)
q2(input)
