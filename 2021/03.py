
with open("03.txt") as fh:
    input = [line.strip() for line in fh.readlines()]

if False:
    input = """00100
    11110
    10110
    10111
    10101
    01111
    00111
    11100
    10000
    11001
    00010
    01010""".split("\n")
    input = [s.strip() for s in input]

last = input[0]

gamma = ""
epsilon = ""

n = len(input[0])

for i in range(n):
    num_zeroes = 0
    num_ones = 0
    for l in input:
        c = l[i]
        if c == "0":
            num_zeroes += 1
        elif c == "1":
            num_ones += 1
        else:
            raise Exception("wrong char " + repr(c) + " in " + l + " at " + str(i))
    if num_ones > num_zeroes:
        gamma += "1"
        epsilon += "0"
    else:
        gamma += "0"
        epsilon += "1"

gamma = int(gamma, 2)
epsilon = int(epsilon, 2)

print(gamma, epsilon, gamma*epsilon)

data = input
n = len(input[0])
for i in range(n):
    num_zeroes = 0
    zeroes= []
    num_ones = 0
    ones = []
    for l in data:
        c = l[i]
        if c == "0":
            num_zeroes += 1
            zeroes.append(l)
        elif c == "1":
            num_ones += 1
            ones.append(l)
        else:
            raise Exception("wrong char " + repr(c) + " in " + l + " at " + str(i))
    if num_ones >= num_zeroes:
        data = ones
    else:
        data = zeroes
    
    if len(data) == 1:
        break

o2 = data[0]

data = input
n = len(input[0])
for i in range(n):
    num_zeroes = 0
    zeroes= []
    num_ones = 0
    ones = []
    for l in data:
        c = l[i]
        if c == "0":
            num_zeroes += 1
            zeroes.append(l)
        elif c == "1":
            num_ones += 1
            ones.append(l)
        else:
            raise Exception("wrong char " + repr(c) + " in " + l + " at " + str(i))
    if num_ones < num_zeroes:
        data = ones
    else:
        data = zeroes

    if len(data) == 1:
        break

co2 = data[0]

print(o2, co2)
o2 = int(o2, 2)
co2 = int(co2, 2)
print(o2, co2, o2*co2)

