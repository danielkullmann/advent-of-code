
testInput = """be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce"""

testInput = [s.strip() for s in testInput.split("\n")]

with open("08.txt") as fh:
    input = fh.read()
    input = [s.strip() for s in input.split("\n") if s.strip() != ""]

def q1(input):
    values = []
    for line in input:
        parts = line.strip().split("|")
        values.extend([ s for s in parts[1].strip().split(" ") if s != ""])
    count = 0
    for v in values:
        vl = len(v)
        if vl == 2 or vl == 4 or vl == 3 or vl == 7:
            count += 1
    print(count)



"""
 0000
1    2
1    2
 3333
4    5
4    5
 6666
"""
from itertools import permutations
def q2(input):
    numbers = {
        frozenset([0, 1, 2, 4, 5, 6]): 0,
        frozenset([2, 5]): 1,
        frozenset([0, 2, 3, 4, 6]): 2,
        frozenset([0, 2, 3, 5, 6]): 3,
        frozenset([1, 2, 3, 5]): 4,
        frozenset([0, 1, 3, 5, 6]): 5,
        frozenset([0, 1, 3, 4, 5, 6]): 6,
        frozenset([0, 2, 5]): 7,
        frozenset([0, 1, 2, 3, 4, 5, 6]): 8,
        frozenset([0, 1, 2, 3, 5, 6]): 9,
    }
    numbers_segments = set(numbers.keys())
    result = 0
    for line in input:
        parts = line.strip().split("|")
        befores = [ set(s) for s in parts[0].strip().split(" ") if s != ""]
        afters = [ set(s) for s in parts[1].strip().split(" ") if s != ""]
        for permutation in permutations("abcdefg"):
            permutation_map = {} # char -> number
            for i in range(len(permutation)):
                permutation_map[permutation[i]] = i
            all_befores_are_numbers = True
            for before in befores:
                alleged_number = frozenset({permutation_map[b] for b in before})
                if not alleged_number in numbers_segments:
                    all_befores_are_numbers = False
                    break
            if all_befores_are_numbers:
                whole_after_number = 0
                for after in afters:
                    after_number = frozenset({permutation_map[a] for a in after})
                    after_number = numbers[after_number]
                    whole_after_number = 10*whole_after_number + after_number
                result += whole_after_number
                break
    print(result)

q1(testInput)
q1(input)
q2(testInput)
q2(input)
