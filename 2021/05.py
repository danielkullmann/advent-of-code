
testInput = """0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2"""

testInput = [s.strip() for s in testInput.split("\n")]

with open("05.txt") as fh:
    input = fh.readlines()
    input = [s.strip() for s in input]

def parse(input):
    result = []
    for line in input:
        parts = line.split("->")
        p1 = parts[0].strip().split(",")
        p2 = parts[1].strip().split(",")
        result.append((
            int(p1[0]),
            int(p1[1]),
            int(p2[0]),
            int(p2[1])
        ))
    return result

testInput = parse(testInput)
input = parse(input)

def q1(input):
    doubles = set()
    seen = set()
    for x1,y1,x2,y2 in input:
        if x1 == x2:
            y1, y2 = min(y1, y2), max(y1, y2)
            for y in range(y1,y2+1):
                pos = (x1, y)
                if pos in seen:
                    doubles.add(pos)
                seen.add(pos)
        if y1 == y2:
            x1, x2 = min(x1, x2), max(x1, x2)
            for x in range(x1,x2+1):
                pos = (x, y1)
                if pos in seen:
                    doubles.add(pos)
                seen.add(pos)
    print(len(seen), len(doubles))

def q2(input):
    doubles = set()
    seen = set()
    for x1,y1,x2,y2 in input:
        if x1 == x2:
            y1, y2 = min(y1, y2), max(y1, y2)
            for y in range(y1,y2+1):
                pos = (x1, y)
                if pos in seen:
                    doubles.add(pos)
                seen.add(pos)
        elif y1 == y2:
            x1, x2 = min(x1, x2), max(x1, x2)
            for x in range(x1,x2+1):
                pos = (x, y1)
                if pos in seen:
                    doubles.add(pos)
                seen.add(pos)
        elif abs(x2-x1) == abs(y2-y1): # diagonal
            dx = 1
            if x1 > x2:
                dx = -1
            dy = 1
            if y1 > y2:
                dy = -1
            x, y = x1, y1
            while True:
                pos = (x, y)
                if pos in seen:
                    doubles.add(pos)
                seen.add(pos)
                if x == x2:
                    break
                x += dx
                y += dy
        else:
            pass
            
    print(len(seen), len(doubles))

q1(testInput)
q1(input)
q2(testInput)
q2(input)
