
testInput = """[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]"""

testInput = [s.strip() for s in testInput.split("\n")]

with open("10.txt") as fh:
    input = fh.read()
    input = [s.strip() for s in input.split("\n") if s.strip() != ""]


def q1(input):
    scores = {
        ")": 3,
        "]": 57,
        "}": 1197,
        ">": 25137
    }
    match = {
        "(": ")",
        "[": "]",
        "{": "}",
        "<": ">"
    }

    opens = set([c for c in "([{<"])
    closes = set([c for c in ")]}>"])

    total_score = 0
    for line in input:
        stack = []
        for c in line:
            if c in opens:
                stack.append(c)
            elif c in closes:
                expected = match[stack.pop()]
                if c != expected:
                    total_score += scores[c]
                    break
            else:
                print("ERROR", c)
    print(total_score)


def q2(input):
    scores = {
        ")": 1,
        "]": 2,
        "}": 3,
        ">": 4
    }
    match = {
        "(": ")",
        "[": "]",
        "{": "}",
        "<": ">"
    }

    opens = set([c for c in "([{<"])
    closes = set([c for c in ")]}>"])

    total_scores = []
    for line in input:
        total_score = 0
        stack = []
        corrupted = False
        for c in line:
            if c in opens:
                stack.append(c)
            elif c in closes:
                expected = match[stack.pop()]
                if c != expected:
                    corrupted = True
                    break
            else:
                print("ERROR", c)
        if not corrupted:
            while stack:
                c = stack.pop()
                total_score = 5*total_score + scores[match[c]]
        if total_score != 0:
            total_scores.append(total_score)
    total_scores.sort()
    print(total_scores[len(total_scores) // 2])

q1(testInput)
q1(input)
q2(testInput)
q2(input)
