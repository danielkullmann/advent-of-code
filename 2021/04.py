
testInput = """7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7"""

testInput = [s.strip() for s in testInput.split("\n")]

with open("04.txt") as fh:
    input = fh.readlines()
    input = [s.strip() for s in input]

def check_rows(card,picked):
    for row in card:
        all = True
        for number in row:
            if number not in picked:
                all = False
                break
        if all:
            return True
    return False

def check_cols(card,picked):
    for col_no in range(len(card[0])):
        all = True
        for row in card:
            number = row[col_no]
            if number not in picked:
                all = False
                break
        if all:
            return True
    return False

def check_card(card,picked):
    return check_rows(card, picked) or check_cols(card, picked)

def parse(input):
    numbers = [int(s) for s in input[0].split(",")]

    cards = []
    card = []
    for line in input[1:]:
        if line == "":
            if len(card) > 0:
                cards.append(card)
                card = []
            continue
        line_numbers = [int(s) for s in line.split(" ") if s != ""]
        card.append(line_numbers)

    if len(card) > 0:
        cards.append(card)

    return numbers, cards

def score(final_number, card, picked):
    card_sum = 0
    for row in card:
        for number in row:
            if number not in picked:
                card_sum += number
    return card_sum, final_number * card_sum

def q1(input):
    numbers, cards = parse(input)
    final_number = None
    winning_card = None

    picked = set()
    finished = False
    for number in numbers:
        if finished: break
        picked.add(number)

        for card in cards:
            if check_card(card,picked):
                final_number = number
                winning_card = card
                finished = True
                break

    print(final_number, score(final_number, winning_card, picked))

def q2(input):
    numbers, cards = parse(input)

    final_number = None
    winning_cards = set()
    losing_card = None
    losing_card_index = None

    picked = set()
    for number in numbers:
        picked.add(number)
        final_number = number

        for index, card in enumerate(cards):
            if index in winning_cards: continue
            if check_card(card,picked):
                winning_cards.add(index)
                losing_card = card
                losing_card_index = index
        if len(winning_cards) == len(cards):
            break
        

    print(final_number, losing_card_index, losing_card, score(final_number, losing_card, picked))

q1(testInput)
print()
q1(input)
print()
print()
q2(testInput)
print()
q2(input)