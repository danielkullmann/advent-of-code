
def process_line(line):
    a, b = line.split(" ")
    return (a, int(b))

with open("02.txt") as fh:
    input = [process_line(line) for line in fh.readlines()]

depth = 0
pos = 0
for command,value in input:
    if command == "forward":
        pos += value
    elif command == "down":
        depth += value
    elif command == "up":
        depth -= value
    else:
        raise Exception("Unknown command", command)

print(depth, pos)
print(depth*pos)

depth = 0
pos = 0
aim = 0
for command,value in input:
    if command == "forward":
        pos += value
        depth += aim * value
    elif command == "down":
        aim += value
    elif command == "up":
        aim -= value
    else:
        raise Exception("Unknown command", command)

print(depth, pos)
print(depth*pos)
