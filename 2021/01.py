
with open("01.txt") as fh:
    input = [int(line) for line in fh.readlines()]

#input = [199,200,208,210,200,207,240,269,260,263]

last = input[0]

incs = 0
for next in input[1:]:
    if next > last:
        incs += 1
    last = next

print(incs)


window = sum(input[0:3])

incs = 0
for i in range(3, len(input)):
    new_window = window + input[i] - input[i-3]
    #print(window, input[i-3], input[i], "=>", new_window)
    if new_window > window:
        incs += 1
    window = new_window

print(incs)
