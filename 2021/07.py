
testInput = """16,1,2,0,4,2,7,1,2,14"""

testInput = [int(s.strip()) for s in testInput.split(",")]

with open("07.txt") as fh:
    input = fh.read()
    input = [int(s.strip()) for s in input.split(",")]

def q1(input):
    minv, maxv = min(input), max(input)
    min_fuel = None
    level = None
    for l in range(minv, maxv+1):
        fuel = 0
        for k in input:
            fuel += abs(k-l)
        if min_fuel is None or fuel < min_fuel:
            min_fuel = fuel
            level = l
    print(min_fuel, level)

def calc(v):
    if v == 0:
        return 0
    return v * (v+1) // 2

def q2(input):
    minv, maxv = min(input), max(input)
    min_fuel = None
    level = None
    for l in range(minv, maxv+1):
        fuel = 0
        for k in input:
            fuel += calc(abs(k-l))
        if min_fuel is None or fuel < min_fuel:
            min_fuel = fuel
            level = l
    print(min_fuel, level)

q1(testInput)
q1(input)
q2(testInput)
q2(input)
